'use client';
import { sStoreCtx } from '@/assets/context';

import { L } from 'libs/at';
import cx from 'classnames';
import { toSvg } from 'html-to-image';
import { keys, map, toString } from 'lodash';
import QRCode from 'react-qr-code';
import styles from './styles.module.scss';

export default function Index() {
  const { json } = sStoreCtx.use();

  const contact = json?.total?.contact || {};
  const visit = json?.total?.visit || {};

  const onDownload = async () => {
    try {
      const before = document.getElementById('before');
      const after = document.getElementById('after');

      if (before && after) {
        const dataUrlBefore = await toSvg(before);
        const dataUrlAfter = await toSvg(after);

        for (const key of [dataUrlAfter, dataUrlBefore]) {
          if (key) {
            // const filename = `${v4()}.svg`;
            // _on.Download({ url: key, filename });
          }
        }
      }
    } catch (error) {
      console.info('error :>> ', error);
    }
  };

  return (
    <L.Contain className={styles.Contact} isScrolling>
      <L.Yard gap={16} vertical justifyContent="center" flex={1} padding={16}>
        <L.Cover gap={16}>
          <L.Txt
            alignItems="center"
            textTransform="uppercase"
            fontSize={18}
            fontWeight={600}>
            Edit card
          </L.Txt>
          <L.Btn onClick={onDownload} kindType="outline">
            Download
          </L.Btn>
        </L.Cover>

        <L.Section flex={1} width={'100%'} gap={16} flexWrap="wrap">
          <L.Card
            hover
            id="before"
            className={cx(styles.cardID, styles.before)}>
            <L.Cover className={styles.title}>
              <L.Txt className={cx(styles.txt, styles.career)}>
                {visit?.career}
              </L.Txt>
              <L.Txt className={cx(styles.txt, styles.job)}>{visit?.job}</L.Txt>
              <L.Txt className={cx(styles.txt, styles.name)}>
                {visit?.name}
              </L.Txt>
            </L.Cover>

            <L.Cover className={styles.QRcode}>
              <QRCode size={80} value={toString(contact?.qrcode?.value)} />
            </L.Cover>
          </L.Card>

          <L.Card hover id="after" className={cx(styles.cardID, styles.after)}>
            <L.Cover className={styles.info}>
              <L.List column={1} className={styles.lstInfo}>
                {map(
                  keys(contact),
                  (item: keyof typeof contact) =>
                    contact?.[item]?.icon && (
                      <L.Item key={item} className={styles.item}>
                        <L.Cover
                          hover
                          gap={8}
                          padding={8}
                          letterSpacing={1}
                          borderRadius={0}
                          className={styles.line}>
                          <L.Icon
                            width={20}
                            height={20}
                            fontSize={20}
                            dangerouslySetInnerHTML={{
                              __html: contact?.[item]?.icon,
                            }}
                            color="lightcoral"
                          />
                          <L.Txt fontWeight={500} wordBreak="break-word">
                            {contact?.[item]?.value}
                          </L.Txt>
                        </L.Cover>
                      </L.Item>
                    ),
                )}
              </L.List>

              <L.Cover className={styles.QRcode}>
                <QRCode size={120} value={toString(contact?.qrcode?.value)} />
              </L.Cover>
            </L.Cover>
          </L.Card>
        </L.Section>
      </L.Yard>
    </L.Contain>
  );
}
