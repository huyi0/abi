'use client';
import { ModalCtx, sStoreCtx } from '@/assets/context';
import { $Json } from '@/assets/types/json';
import { $ModalKeys } from '@/collects/modals';
import { L } from 'libs/at';
import { map } from 'lodash';
import Link from 'next/link';
import { memo } from 'react';

export default memo(function Index() {
  const { json } = sStoreCtx.use();

  const { onToggle } = ModalCtx.use();

  const onActionItem = (item: $Json['total']['menu'][0]) => {
    if (item.modal) {
      onToggle({
        key: item.modal as $ModalKeys,
        data: {
          sum(a = 0, b = 0) {
            return a + b;
          },
        },
      });
    }
  };

  const column = 3;

  return (
    <L.List column={column} padding={0} id="listMenu">
      {map(json?.total?.menu, (item, idx) => {
        return (
          <L.Item key={idx} onClick={() => onActionItem(item)} padding={0}>
            <Link href={item?.slug}>
              <L.Cover
                shadow
                hover
                width={'100%'}
                aspectRatio={2 / 1}
                alignItems="center"
                justifyContent="flex-start"
                gap={8}
                padding={'5px 8px'}>
                <L.Icon fontSize={20} className={item.icon} />
                <L.Txt fontSize={12} textAlign="left" letterSpacing={2}>
                  {item?.label}
                </L.Txt>
              </L.Cover>
            </Link>
          </L.Item>
        );
      })}
    </L.List>
  );
});
