import { ctrl } from '@/process/ctrl';
import { $Json } from '@/assets/types/json';
import { StateCreator } from 'zustand';

const init = {
  json: {
    Id: '',
  } as $Json,
};

type States = typeof init;

type Actions = {
  setJson: (data: $Json) => void;
  getJson(): Promise<void>;
  resetJson(): void;
};
export type $JsonSlice = States & Actions;

export const jsonSlice: StateCreator<$JsonSlice> = (set, get) => {
  return {
    ...init,
    setJson: (data: $Json) => set({ json: data }),
    async getJson() {
      const rs = await ctrl().getJson();
      set({ json: rs.data });
    },

    resetJson() {
      set(init);
      get().getJson();
    },
  };
};
