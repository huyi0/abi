import About from './About';
import Blog from './Blog';
import Chart from './Chart';
import Contact from './Contact';
import EJson from './EJson';
import EditContact from "./EditContact";
import Home from './Home';
import Masonry from './Masonry';
import Media from './Media';
import Notification from './Notification';
import Product from './Product';
import TestA from './TestA';
import TestB from './TestB';
import TestC from './TestC';
import TestTbl from './TestTbl';

export {
  About,
  Blog,
  Chart,
  Contact,
  EJson,
  EditContact,
  Home,
  Masonry,
  Media,
  Notification,
  Product,
  TestA,
  TestB,
  TestC,
  TestTbl
};

