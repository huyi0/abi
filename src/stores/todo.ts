import { StateCreator } from 'zustand';

export interface ITodo {
  todos: Array<string>;
  addTodo: (todo: string) => void;
  clearTodo: () => void;
}

export const todoSlice: StateCreator<ITodo> = (set) => ({
  todos: ['create', 'next js app', 'using typescript'],
  addTodo(todo: string) {
    set((state) => ({ todos: [...state.todos, todo] }));
  },
  clearTodo() {
    set({ todos: [] });
  },
});
