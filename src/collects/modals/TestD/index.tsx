import { $Modal } from 'libs/es';
import { L } from 'libs/at';
import styles from './styles.module.scss';
import { FormProvider, useForm } from 'react-hook-form';

export default function Index({ open, onToggle, ...args }: $Modal) {
  return (
    <L.Modal
      action={{ onToggle }}
      set={{
        clsModal: styles.modal,
        clsOverlay: styles.overlay,
        open,
        isCloseOutside: true,
        header: (props) => {
          return <Header {...props} {...args} />;
        },
        content: () => {
          return <Content />;
        },
      }}
    />
  );
}

const Header = ({ ...props }: any) => {
  return <button onClick={props.onToggle}>X</button>;
};

const Content = () => {
  const methods = useForm({ mode: 'all' });
  const { handleSubmit } = methods;
  const onSave = (data: any) => {
    const channelC = new BroadcastChannel('C');
    channelC.postMessage(data);
  };

  return (
    <FormProvider {...methods}>
      <h2>Modal Content</h2>
      <L.Block vertical gap={16}>
        <L.Cover vertical gap={16}>
          <L.TxtField name="a" label={'a'} />
          <L.TxtField name="b" label={'b'} />
          <L.TxtField name="c" label={'c'} />
        </L.Cover>

        <L.Btn
          width={'fit-content'}
          kindType="outline"
          onClick={handleSubmit(onSave)}>
          Save
        </L.Btn>
      </L.Block>
    </FormProvider>
  );
};
