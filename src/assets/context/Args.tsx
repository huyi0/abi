'use client';
import { args } from '@/init';
import { $Env } from '@/process/env/tEnv';
import { onGumCtx } from './GumCtx';

type Props = {
  env: $Env;
};

export const { Provider, Consumer, use } = onGumCtx<
  ReturnType<typeof useLogic>,
  Props
>({
  useLogic,
});

function useLogic({ env }: Props) {
  args.env = env;
  return { env };
}
