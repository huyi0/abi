'use client';
import { useForm } from 'react-hook-form';
import { onGumCtx } from './GumCtx';

export const { Provider, Consumer, use } = onGumCtx<
  ReturnType<typeof useLogic>
>({
  useLogic,
});

function useLogic() {
  const method = useForm({
    mode: 'onChange',
    defaultValues: {},
  });

  return {
    a: 'helo',
    method,
  };
}
