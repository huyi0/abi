export const _on = {
  async Copy(txt: string) {
    if (typeof navigator.clipboard === 'undefined') {
      const textArea = document.createElement('textarea');
      textArea.value = txt;
      textArea.style.position = 'fixed';
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();
      document.execCommand('copy');
      document.body.removeChild(textArea);
      return true;
    } else {
      navigator.clipboard?.writeText && navigator.clipboard?.writeText(txt);
      return false;
    }
  },

  Download({ url = '', filename = '' }) {
    const link = document.createElement('a');
    link.href = url;
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  },
};
