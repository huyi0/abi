import { ctrl } from '@/process/ctrl';
import { $Blog } from '@/assets/types/blog';
import { StateCreator } from 'zustand';

const init = {
  blogs: [],
  blog: {},
};
export interface State {
  onBlogs: () => Promise<void>;
  blogs: $Blog[];
  blog: $Blog;
  pickBlog(Blog: $Blog): void;
}

export const blogSlice: StateCreator<State> = (set) => {
  return {
    ...init,
    async onBlogs() {
      const rs = await ctrl().getBlogs();
      set({ blogs: rs.data });
    },
    pickBlog({ blog }: { blog: object }) {
      set({ blog });
    },
  } as const;
};
