'use client';
import { L } from 'libs/at';
import { ctrl } from '@/process/ctrl';
import { map } from 'lodash';
import React from 'react';
import styles from './styles.module.scss';

export default function Index() {
  const [fules, setFules] = React.useState({} as any);

  const onGetList = async () => {
    const rs: any = await ctrl().onGetGoldPrice();
    setFules(rs);
  };

  React.useEffect(() => {
    onGetList();
  }, []);

  return (
    <L.Legend
      label={<L.Txt color="lightblue">Giá vàng</L.Txt>}
      shadow
      className={styles.block}
      vertical
      borderColor="lightgray"
      padding={'1rem'}
      gap={'1rem'}>
      <L.Box>
        <L.Txt color="lightsalmon">
          Cập nhật ngày {fules?.ratelist?.[0]?.$?.updated}
        </L.Txt>
      </L.Box>

      <L.List column={1}>
        {map(fules?.ratelist?.[0]?.city, (itm, idx) => {
          return (
            <L.Item key={idx} vertical gap="0.5rem">
              <L.Legend
                label={<L.Txt color="lightsalmon">{itm?.$?.name}</L.Txt>}
                borderColor="lightgray">
                <>
                  <L.List column={1} overflow="auto">
                    <L.Item>
                      <L.Cover gap={'1rem'} fontWeight={600} flex={1}>
                        <L.Txt minWidth={'15%'}>Giá mua</L.Txt>
                        <L.Txt minWidth={'15%'}>Giá bán</L.Txt>
                        <L.Txt minWidth={'15%'}>+/-</L.Txt>
                        <L.Txt>Loại</L.Txt>
                      </L.Cover>
                    </L.Item>
                    {map(itm?.item, (ele, id) => {
                      return (
                        <L.Item key={id}>
                          <L.Cover gap={'1rem'} flex={1}>
                            <L.Txt minWidth={'15%'}>{ele?.$?.buy}</L.Txt>
                            <L.Txt minWidth={'15%'}>{ele?.$?.sell}</L.Txt>
                            <L.Txt minWidth={'15%'}>
                              {(ele?.$?.sell - ele?.$?.buy).toFixed(3)}
                            </L.Txt>
                            <L.Txt>{ele?.$?.type.replace('Vàng', '')}</L.Txt>
                          </L.Cover>
                        </L.Item>
                      );
                    })}
                  </L.List>
                </>
              </L.Legend>
            </L.Item>
          );
        })}
      </L.List>
    </L.Legend>
  );
}
