import List from '@/collects/list';
import { L } from 'libs/at';
import styles from './styles.module.scss';

export default function Home() {
  return (
    <L.Contain className={styles.Home} overflow="auto" vertical gap={16}>
      <L.Yard horizontal flex={1} className={styles.yard} gap={'1rem'}>
        <L.Column className={styles.column} flex={1}>
          <L.Cover>
            <L.Txt className={styles.title}>Công nghệ số</L.Txt>
          </L.Cover>
          <List.Posts />
        </L.Column>
        <L.Column className={styles.column} flex={1}>
          <List.FuelPrices />
          <List.Gold />
        </L.Column>
      </L.Yard>
    </L.Contain>
  );
}
