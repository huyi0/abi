'use client';
import { persions } from '@/assets/mock/person';
import { L } from 'libs/at';
import { $TblColumn } from 'libs/by/Table';
import { clone, size } from 'lodash';
import Pagination from 'rc-pagination';
import { ChangeEvent, Key, useState } from 'react';
import { useForm } from 'react-hook-form';

export default function Screen() {
  const pageSize = 10;
  const [rowKeys, setRowKeys] = useState<Key[]>([]);
  const [pagePagi, setPagePagi] = useState({
    page: 1,
    pageSize: pageSize,
  });

  // useTableFilters({ list: persions });
  const methods = useForm({ mode: 'all' });

  return (
    <L.FormFields methods={methods}>
      <L.Contain flex={1} display="flex">
        <L.Cover flex={1} vertical gap={'1rem'}>
          <L.Txt tag="h1">My Table</L.Txt>
          <L.Cover flex={1}>
            <L.Cover isScrolling flex={1} height={'450px'}>
              <L.Table
                keyID={'ID'}
                dataSource={clone(persions).slice(
                  pagePagi.pageSize * (pagePagi.page - 1),
                  pagePagi.page * pagePagi.pageSize,
                )}
                columns={columns()}
                selection={{
                  frozen: true,
                  rowKeys,
                  onChange({ rowKeys }) {
                    setRowKeys(rowKeys);
                  },
                  width: 40,
                  selectionUI: ({ onChange, isChecked }) => {
                    return (
                      <L.Tick
                        sizes="1.25rem"
                        {...{
                          isChecked,
                          onTick() {
                            onChange?.();
                          },
                        }}
                      />
                    );
                  },
                }}
                onRows={({ record }) => {
                  return {
                    onClick() {
                      setRowKeys([record.ID]);
                    },
                  };
                }}
                extraFooter={() => {
                  return (
                    <L.Block center gap={'1rem'}>
                      <Pagination
                        pageSize={pageSize}
                        total={size(clone(persions))}
                        current={pagePagi.page}
                        onChange={(page, pageSize) => {
                          setPagePagi({ page, pageSize });
                        }}
                      />

                      <L.Cover>
                        <L.TxtField
                          type="number"
                          name="jumper"
                          label={'Hiển thị: '}
                          value={pagePagi.pageSize}
                          onChange={(e: ChangeEvent<HTMLInputElement>) => {
                            setPagePagi((prev) => ({
                              ...prev,
                              pageSize: Number(e.target?.value),
                            }));
                          }}
                        />
                      </L.Cover>
                    </L.Block>
                  );
                }}
              />
            </L.Cover>
          </L.Cover>
        </L.Cover>
        <L.Cover flex={1} vertical>
          <L.Txt>hello</L.Txt>

          <L.Collapse
            begin={({ onToggle }) => {
              return (
                <L.Btn onClick={onToggle} shadow>
                  <L.Txt>Collapse</L.Txt>
                </L.Btn>
              );
            }}
            content={() => {
              return (
                <L.Card>
                  <L.Txt>content</L.Txt>
                </L.Card>
              );
            }}
          />
        </L.Cover>
      </L.Contain>
    </L.FormFields>
  );
}

const columns = () => {
  return [
    {
      key: 'ID',
      title: 'ID',
      width: 50,
      frozen: true,
    },

    {
      key: 'Name',
      title: () => {
        return <p>Name</p>;
      },
      width: 150,
      frozen: true,
      render({ value }) {
        return <b>{value}</b>;
      },
    },
    {
      key: 'Age',
      title: 'Age',
      width: 100,
    },
    {
      key: 'Address',
      title: 'Address',
      width: 200,
      frozen: true,
      render(agrs) {
        return <center>{agrs.value}</center>;
      },
    },
    { key: 'Occupation', title: 'Occupation', width: 150 },
    { key: 'Email', title: 'Email', width: 200 },
    { key: 'Phone', title: 'Phone', width: 150 },
    { key: 'Company', title: 'Company', width: 150 },
    { key: 'Salary', title: 'Salary', width: 100 },
  ] as $TblColumn[];
};
