'use client';
import List from '@/collects/list';
import { L } from 'libs/at';

export default function Index() {
    return (
        <L.Contain>
            <List.Blogs />
        </L.Contain>
    );
}
