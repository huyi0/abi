'use client';
import { L } from 'libs/at';

export default function Index() {
  return (
    <L.Section padding={16} flex={1} flexDirection="column" isScrolling>
      <L.Txt fontStyle="italic">Đang phát triễn ...</L.Txt>
      <L.Cover minHeight={'100vh'} />
      <L.Cover>
        <L.Dropbox
          isOutlet
          impact={({ onToggle }) => {
            return (
              <L.Cover>
                <L.Txt hover onClick={onToggle}>
                  Calendar
                </L.Txt>
              </L.Cover>
            );
          }}
          contents={() => {
            return (
              <L.Card background={'var(--background)'} padding={'0'}>
                <L.Calendar />
              </L.Card>
            );
          }}
        />
      </L.Cover>{' '}
      <L.Cover minHeight={'100vh'} />
    </L.Section>
  );
}
