'use client';
import { CtxEjson, sStoreCtx } from '@/assets/context';
import { JsonEditor } from 'json-edit-react';
import { L } from 'libs/at';
import styles from './styles.module.scss';

export default function Page() {
  const { json } = sStoreCtx.use();

  const { onUpdate, collapse } = CtxEjson.use();

  return (
    <>
      <L.Block
        padding={0}
        overflow="auto"
        flex={1}
        vertical
        position="relative">
        <JsonEditor
          className={styles.JsonEditor}
          maxWidth={'100vw'}
          key={'database'}
          rootName="datatbase:::"
          data={json}
          collapse={collapse}
          onUpdate={onUpdate}
          enableClipboard
        />
      </L.Block>
    </>
  );
}
