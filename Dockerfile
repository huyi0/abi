FROM node:alpine AS base

FROM base AS builder
RUN apk add --no-cache libc6-compat
WORKDIR /web_abi
COPY package*.json yarn.lock* ./
RUN yarn --frozen-lockfile --production
COPY . .
RUN yarn build

FROM base AS runner
WORKDIR /web_abi
COPY --from=builder /web_abi/.next/standalone ./
COPY --from=builder /web_abi/.next/static ./.next/static
COPY --from=builder /web_abi/public ./public
COPY --from=builder /web_abi/sEnv.js ./sEnv.js

CMD node sEnv.js && node server.js 