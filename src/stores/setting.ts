import { Mode } from '@/assets/types';
import { StateCreator } from 'zustand';

type ModeKeys = keyof typeof Mode;

export interface Setting {
  mode: ModeKeys;
  onMode(mode: ModeKeys): void;
}

export const settingSlice: StateCreator<Setting> = (set) =>
  ({
    mode: Mode.light,
    onMode(mode: ModeKeys) {
      set({ mode });
    },
  }) as const;
