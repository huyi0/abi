import Blogs from './Blogs';
import FuelPrices from './FuelPrices';
import Gold from './Gold';
import Menu from './Menu';
import Posts from './Posts';

const List = {
  Posts,
  FuelPrices,
  Blogs,
  Menu,
  Gold,
};

export default List;
