'use client';
import { sStore } from '@/stores';
import { useEffect } from 'react';
import { onGumCtx } from './GumCtx';

export const { Provider, Consumer, use } = onGumCtx<
  ReturnType<typeof useLogic>
>({
  useLogic,
});

function useLogic() {
  const { getJson } = sStore();

  useEffect(() => {
    getJson();
  }, []);

  return { ...sStore() };
}
