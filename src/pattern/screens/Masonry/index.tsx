'use client';
import { filter, map, slice } from 'lodash';
import { memo, useEffect, useRef, useState } from 'react';
import styles from './styles.module.scss';
import { L } from 'libs/at';

export default memo(function Test() {
  const [limit, setLimit] = useState(5);

  const column = 3;

  const list = map(Array.from({ length: 200 }).fill(0), (_, idx) => {
    return {
      index: idx,
      label: idx,
      image: 'https://source.unsplash.com/random?t=' + idx,
    };
  });

  const dimension = Array.from({ length: column }, (_, colIndex) =>
    filter(slice(list, 0, limit), (_, index) => index % column === colIndex),
  );

  const hasMore = true;
  const pageEndRef = useRef(null);

  useEffect(() => {
    if (hasMore) {
      const observer = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          // kiểm tra element có nằm trong viewport không?
          setLimit((prev) => prev + 15);
        }
      });

      if (pageEndRef.current) {
        observer.observe(pageEndRef.current);
      }

      return () => {
        if (pageEndRef.current) {
          observer.unobserve(pageEndRef.current);
        }
      };
    }
  }, []);

  return (
    <L.Contain vertical className={styles.about}>
      <L.Yard className={styles.yard} flex={1} vertical>
        <L.Txt fontStyle="italic">Đang phát triễn ...</L.Txt>

        <L.Block gap={16} overflow="auto" margin={'-1rem'} flex={1}>
          <L.List
            padding={'1rem'}
            column={3}
            backgroundColor="transparent"
            isScrolling
            flex={1}>
            {map(dimension, (slice, idx) => {
              return (
                <L.Item key={idx} vertical gap={16}>
                  {map(slice, (item, index) => {
                    return (
                      <L.Card key={index} flex={1} aspectRatio={1}>
                        <L.Txt>{item.label}</L.Txt>
                        <picture style={{ aspectRatio: 1 }}>
                          <img src={item?.image} alt="" />
                        </picture>
                        <div ref={pageEndRef}>more</div>
                      </L.Card>
                    );
                  })}
                </L.Item>
              );
            })}
          </L.List>
        </L.Block>
      </L.Yard>
    </L.Contain>
  );
});
