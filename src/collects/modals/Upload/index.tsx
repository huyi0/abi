import { L } from 'libs/at';
import { ctrl } from '@/process/ctrl';
import { FormProvider, useForm } from 'react-hook-form';
import styles from './styles.module.scss';
import { $Modal } from 'libs/es';

export default function Index({ open, onToggle, ...args }: $Modal) {
  return (
    <L.Modal
      action={{ onToggle }}
      set={{
        clsModal: styles.modal,
        clsOverlay: styles.overlay,
        open,
        isCloseOutside: true,
        header: (props) => {
          return <Header {...props} {...args} />;
        },
        content: () => {
          return <Content />;
        },
      }}
    />
  );
}

const Header = ({ ...props }: any) => {
  return (
    <L.Cover padding={'0'} marginBottom={'1rem'}>
      <L.Txt fontSize={'1.5rem'} fontWeight={500} color="lightseagreen">
        Upload Medias
      </L.Txt>
      <L.Btn
        position="absolute"
        margin={-16}
        top={0}
        right={0}
        className={styles.btnClose}
        width={'fit-content'}
        background={'lightcoral'}
        shadow
        hover
        onClick={props.onToggle}
        color="white">
        X
      </L.Btn>
    </L.Cover>
  );
};

const Content = () => {
  const methods = useForm({ mode: 'all' });
  const { handleSubmit } = methods;

  const onSave = async (data: any) => {
    try {
      const form_data = new FormData();

      form_data.append('Bucket', 'abi');
      form_data.append('Segment', data?.Segment);
      form_data.append('file', data?.File[0], data?.File[0]?.name);

      await ctrl().onUploadFile(form_data);
    } catch (error) {
      console.info('error :>> ', error);
    }
  };

  return (
    <FormProvider {...methods}>
      <L.Block vertical gap={16}>
        <L.Cover vertical gap={16}>
          <L.TxtField autoFocus name="Segment" label={'Segment'} />
          <L.Upload
            name="File"
            label={'Upload File'}
            // maxHeight={'400px'}
            maxWidth={'400px'}
          />
        </L.Cover>

        <L.Btn
          width={'fit-content'}
          kindType="outline"
          onClick={handleSubmit(onSave)}>
          Save
        </L.Btn>
      </L.Block>
    </FormProvider>
  );
};
