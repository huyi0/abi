'use client';
import { $PopupKeys, PopupExports } from '@/collects/popups';
import { keys, map } from 'lodash';
import { Fragment, useState } from 'react';
import { onGumCtx } from './GumCtx';

type $ToggleFn = { key: $PopupKeys; data?: object };

type $Show = {
  [T in $PopupKeys]: {
    open: boolean;
    data?: object;
  };
};

export const { Provider, Consumer, use } = onGumCtx<
  ReturnType<typeof useLogic>
>({
  useLogic,
});

function useLogic() {
  const [show, setShow] = useState({} as $Show);

  const onToggle = (props: $ToggleFn) => {
    const { key, data } = props;
    setShow((prev) => ({
      ...prev,
      [key]: { ...prev[key], open: !prev[key]?.open, data },
    }));
  };

  const onRender = () => {
    const es = PopupExports();

    return map(keys(es), (key: $PopupKeys) => {
      const Render = es[key];

      return (
        show[key] && (
          <Fragment key={key}>
            <Render
              {...{
                ...show[key],
                onToggle: () => onToggle({ key }),
              }}
            />
          </Fragment>
        )
      );
    });
  };

  return { onToggle, show, setShow, onRender };
}
