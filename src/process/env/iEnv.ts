import { ajax } from 'jquery';
import { $Env } from './tEnv';

const iEnv = (): $Env | null => {
  try {
    return ajax({
      url: '/js/env.json',
      method: 'GET',
      async: false,
    })?.responseJSON;
  } catch (error) {
    // console.info('error :>> ', error);
    return null;
  }
};

export default iEnv();
