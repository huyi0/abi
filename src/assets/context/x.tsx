import { createContext, useContext } from 'react';

const Ctx = createContext({} as any);
const useCtx = () => useContext(Ctx);
const provider = ({ ...props }: any) => {
  const valueCtx = props?.useLogic({ ...props });
  return (
    <Ctx.Provider value={{ ...valueCtx }}>
      <>{props.children}</>
    </Ctx.Provider>
  );
};

export { Ctx, useCtx, provider };
