'use client';
import { ModalCtx, sStoreCtx } from '@/assets/context';
import { L } from 'libs/at';
import { map } from 'lodash';
import moment from 'moment';
import { useEffect } from 'react';
import styles from './styles.module.scss';

export default function Index() {
  const { pickPost, onPosts, posts } = sStoreCtx.use();
  const { onToggle } = ModalCtx.use();

  useEffect(() => {
    onPosts();
  }, [onPosts]);

  return (
    <L.Block flex={1} className={styles.block}>
      <L.Cover isScrolling>
        <L.List padding={16} column={1} className={styles.List}>
          {map(posts, (item, idx) => {
            return (
              <L.Item key={idx} className={styles.Item}>
                <L.Card
                  shadow={false}
                  flex={1}
                  hover
                  vertical
                  className={styles.Card}
                  onClick={() => {
                    onToggle({ key: 'PostDetails' });
                    pickPost(item);
                  }}>
                  <L.Block gap={'1rem'}>
                    <L.Column>
                      <L.Box
                        width={'50px'}
                        aspectRatio={1}
                        backgroundColor="lightblue"
                        justifyContent="center"
                        alignItems="center"
                        borderRadius={'.5rem'}>
                        <L.Txt color="white">
                          {moment(item?.pubDate).format('HH:mm')}
                        </L.Txt>
                      </L.Box>
                    </L.Column>

                    <L.Column flex={1} gap={8}>
                      <L.Cover aspectRatio={0}>
                        <L.Txt
                          fontSize={12}
                          letterSpacing={1}
                          justifyContent="center"
                          alignItems="center">
                          {moment(item?.pubDate).format('DD/MM/YYYY')}
                        </L.Txt>
                      </L.Cover>
                      <L.Txt
                        letterSpacing={1 / 2}
                        fontWeight={500}
                        fontSize={12}
                        minWidth={'unset'}
                        dangerouslySetInnerHTML={{
                          __html: item?.title,
                        }}></L.Txt>
                    </L.Column>
                  </L.Block>
                </L.Card>
              </L.Item>
            );
          })}
        </L.List>
      </L.Cover>
    </L.Block>
  );
}
