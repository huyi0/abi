'use client';
import { ctrl } from '@/process/ctrl';
import { _on } from '@/process/ons';
import { UpdateFunction } from 'json-edit-react';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { sStoreCtx } from '.';
import { onGumCtx } from './GumCtx';

export const { Provider, Consumer, use } = onGumCtx<ReturnType<typeof useLogic>>({
 useLogic,
});

function  useLogic() {
  const { json, setJson } = sStoreCtx.use();
  const [collapse, setCollapse] = useState(false);

  const onToggleCollapse = () => {
    setCollapse(!collapse);
  };

  const onUpdate: UpdateFunction = ({ newData }) => {
    ctrl().onJsonUpdate({
      Data: newData,
      async cb() {
        const rs = await ctrl().getJson();
        setJson(rs.data);
      },
    });
  };

  const onCopyJson = () => {
    _on
      .Copy(JSON.stringify(json, undefined, 2))
      .then(() => toast.success('Copy success'));
  };

  return { onToggleCollapse, onUpdate, collapse, onCopyJson };
}