export const json = {
  total: {
    menu: [
      {
        slug: '/',
        label: 'Home',
        icon: 'fas fa-house-user',
        modal: '',
      },
      {
        slug: '/about',
        label: 'About',
        icon: 'fa-solid fa-person',
        modal: '',
      },
      {
        slug: '/blog',
        label: 'Blog',
        action: '',
        icon: 'fa-solid fa-blog',
        modal: '',
      },
      {
        slug: '/product',
        label: 'Prod',
        icon: 'fa-solid fa-list-check',
        modal: '',
      },
      {
        slug: '/contact',
        label: 'CardID',
        icon: 'fa-regular fa-address-book',
        modal: '',
      },
      {
        slug: '',
        label: 'Ejson',
        icon: 'fa-solid fa-code',
        modal: 'Ejson',
      },
      {
        slug: '',
        label: 'Dev',
        modal: 'Dev',
        icon: 'fa-brands fa-dev',
      },
      {
        slug: '/masonry',
        label: 'List',
        icon: 'fa-brands fa-dev',
        modal: '',
      },
      {
        slug: '/test',
        label: 'Test',
        icon: 'fa-brands fa-dev',
        modal: '',
      },
      {
        slug: '/notification',
        label: 'Bell',
        icon: 'fa-regular fa-bell',
        modal: '',
      },
    ],
    contact: {
      phone: {
        value: '0583538872',
        icon: '<img src="https://i0.wp.com/help.zalo.me/wp-content/uploads/2023/08/cropped-hinhZalo-2.png?w=177&ssl=1" alt="" />',
      },
      email: {
        icon: '<i class="fa-solid fa-at"></i>',
        value: 'huyi.isr@gmail.com',
      },
      address: {
        value: 'Binh Tan, Ho Chi Minh',
        icon: '<i class="fa-solid fa-location-pin"></i>',
      },
      facebook: {
        value: 'facebook.com/huyi.ss',
        icon: '<i class="fa-brands fa-facebook-f"></i>',
      },
      linkedin: {
        value: '...',
        icon: '<i class="fa-brands fa-linkedin-in"></i>',
      },
      qrcode: {
        value: 'https://abi.sshop.live/contact',
        icon: '',
      },
    },
    visit: {
      career: 'Fontend Developer',
      job: 'UX/UI - FE Middle',
      name: 'HUYI',
      images: {
        before: {},
        after: {},
      },
    },
  },
  posts: [
    {
      id: 'adf8e3d5-b1c7-4a11-99f0-33982ce27854',
      title: 'helo',
    },
    {
      id: '40e8f900-46af-4446-a48f-ba1e3ce30d74',
      title: 'title',
    },
    {
      id: 'adf8e3d5-b1c7-4a54',
      title: 'helo dcscs',
    },
  ],
  Id: '4',
};
