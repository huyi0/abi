import { StateCreator } from 'zustand';

const init = {
  cinemas: [],
  cinema: {},
};

type $Cinema = {
  [T: string]: string;
};
export interface State {
  onCinemas: () => Promise<void>;
  cinemas: $Cinema[];
  cinema: $Cinema;
  pickCinema(cinema: $Cinema): void;
}

export const cinemaSlice: StateCreator<State> = (set) => {
  return {
    ...init,
    async onCinemas() {
      set({ cinemas: [] });
    },
    pickCinema(cinema: $Cinema) {
      set({ cinema });
    },
  } as const;
};
