import { args } from '@/init';
import axios, { AxiosInstance } from 'axios';

export const api = axios.create({
  headers: {
    accept: '*/*',
    'Content-Type': 'application/json',
  },
  method: 'options',
}) as AxiosInstance & Record<string, any>;

api.interceptors.request.use(
  async function (config) {
    try {
      config.baseURL = args.env?.BE_URL;
      return config;
    } catch (error) {
      console.error('error xx :>> ', error);
      return config;
    }
  },
  function (error) {
    console.error('error request :>> ', error);
    return Promise.reject(error);
  },
);
