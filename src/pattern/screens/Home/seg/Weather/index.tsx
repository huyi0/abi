import { L } from 'libs/at';
import { useEffect } from 'react';

export default function Index() {
  useEffect(() => {
    async function fetchData() {
      try {
        const position = await getLocation();
        console.info('position :>> ', position);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    }

    fetchData();
  }, []);

  return <L.Master></L.Master>;
}

export type Position = any;

function getLocation(): Promise<Position> {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject);
  });
}
