type Person = {
  ID: number;
  Name: string;
  Age: number;
  Address: string;
  Occupation: string;
  Email: string;
  Phone: string;
  Company: string;
  Salary: string;
};

const names = [
  'Nguyễn Văn A',
  'Trần Thị B',
  'Lê Văn C',
  'Phạm Thị D',
  'Hoàng Văn E',
  'Vũ Thị F',
  'Đỗ Văn G',
  'Ngô Thị H',
  'Bùi Văn I',
  'Phan Thị J',
];
const streets = [
  'Nguyễn Trãi',
  'Trần Hưng Đạo',
  'Lý Thường Kiệt',
  'Phố Huế',
  'Bạch Mai',
  'Đê La Thành',
  'Giải Phóng',
  'Cầu Giấy',
  'Xuân Thủy',
  'Hoàng Quốc Việt',
];
const occupations = [
  'Kỹ sư',
  'Bác sĩ',
  'Luật sư',
  'Giáo viên',
  'Y tá',
  'Quản lý',
  'Nhân viên bán hàng',
  'Tư vấn',
  'Lập trình viên',
  'Thiết kế',
];
const companies = [
  'Công ty ABC',
  'Bệnh viện XYZ',
  'Văn phòng luật sư',
  'Công ty giáo dục',
  'Công ty y tế',
  'Công ty bán lẻ',
  'Công ty tư vấn',
  'Công ty công nghệ',
  'Công ty thiết kế',
  'Công ty marketing',
];

const getRandomElement = (arr: string[]) =>
  arr[Math.floor(Math.random() * arr.length)];
const getRandomNumber = (min: number, max: number) =>
  Math.floor(Math.random() * (max - min + 1)) + min;
const generateEmail = (name: string) =>
  name.toLowerCase().split(' ').join('.') + '@example.com';

export const persions: Person[] = Array.from({ length: 50 }, (_, i) => ({
  ID: i + 1,
  Name: getRandomElement(names),
  Age: getRandomNumber(20, 65),
  Address: `${getRandomNumber(100, 999)} ${getRandomElement(streets)}`,
  Occupation: getRandomElement(occupations),
  Email: generateEmail(getRandomElement(names)),
  Phone: `555-${getRandomNumber(1000, 9999)}`,
  Company: getRandomElement(companies),
  Salary: `$${getRandomNumber(50000, 120000).toLocaleString()}`,
}));
