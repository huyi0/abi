import type { NextRequest } from 'next/server';
import { NextResponse } from 'next/server';

export async function middleware(request: NextRequest) {
  const headers = new Headers(request.headers);
  headers.set('x-url-origin', request.nextUrl.origin);
  const response = NextResponse.next({ headers });
  return response;
}
