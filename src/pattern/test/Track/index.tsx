import React, { useRef, useEffect } from 'react';
import { createPortal } from 'react-dom';
import styles from './styles.module.scss';

const TrackPosition = () => {
  const ARef = useRef<HTMLDivElement>(null);
  const BRef = useRef<HTMLDivElement>(null);

  const updatePosition = () => {
    if (ARef.current && BRef.current) {
      const rect = ARef.current.getBoundingClientRect();
      const position = {
        top: rect.top + window.scrollY + 15,
        left: rect.left + window.scrollX + 15,
      };
      BRef.current.style.transform = `translate(${position.left}px, ${position.top}px)`;
    }
  };

  useEffect(() => {


    requestAnimationFrame(updatePosition)
    window.addEventListener('scroll', updatePosition, { passive: true });
    window.addEventListener('wheel', updatePosition, { passive: true });
    window.addEventListener('resize', updatePosition);

    updatePosition(); // Initial update

    return () => {
      window.removeEventListener('scroll', updatePosition);
      window.removeEventListener('resize', updatePosition);
    };
  }, []);

  return (
    <div className={styles.scrollContainer}>
      <div ref={ARef} style={{ position: 'relative' }}>
        Element A
      </div>
      {createPortal(
        <div
          ref={BRef}
          className={styles.elementB}
          style={{ position: 'absolute', top: 0 }}>
          Element B
        </div>,
        document.body,
      )}
    </div>
  );
};

export default TrackPosition;
