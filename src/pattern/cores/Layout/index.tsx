import { args } from '@/init';
import { L } from 'libs/at';
import styles from './styles.module.scss';

export default function Index({ children }: any) {
  return (
    <L.Core
      flex={1}
      className={styles.layout}
      backgroundImage={`url(${args.env?.LOGO})`}>
      <L.Core gap={'1rem'} tag="section" className={styles.wrapper}>
        {children}
      </L.Core>
    </L.Core>
  );
}
