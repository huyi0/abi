import { StateCreator } from 'zustand';

export type State = {
  setAuth: (data: object) => void;
  getAuth: () => Promise<void>;
  reset: () => void;
  auth: Partial<object>;
};

export const authSlice: StateCreator<State> = (set) => {
  return {
    auth: {},
    setAuth: (data: object) => set({ auth: data }),
    async getAuth() {},

    reset() {},
  } as const;
};
