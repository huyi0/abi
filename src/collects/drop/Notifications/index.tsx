import { sStoreCtx } from '@/assets/context';
import { L } from 'libs/at';
import { map, size } from 'lodash';
import moment from 'moment';
import styles from './styles.module.scss';

export default function Index() {
  const { notifications } = sStoreCtx.use();

  return (
    <L.Block className={styles.BlockNoti}>
      {size(notifications) < 1 ? (
        <L.Card background={'var(--background)'}>
          <L.Txt>Pạn chưa có thông nào!</L.Txt>
        </L.Card>
      ) : (
        <L.Card
          minWidth={350}
          maxHeight={500}
          background={'var(--background)'}
          isScrolling>
          <L.List vertical minWidth={'fit-content'}>
            {map(notifications, ({ data }, idx) => {
              return (
                <L.Item key={idx} flex={1}>
                  <L.Card
                    shadow={false}
                    hover
                    horizontal
                    alignItems="center"
                    border={'0.1px solid lightblue'}
                    flex={1}>
                    <L.Box>
                      <L.Icon fontSize={26} className="fa-regular fa-bell" />
                    </L.Box>
                    <L.Cover vertical gap={'0.5rem'}>
                      <L.Txt
                        fontWeight={600}
                        fontSize={14}
                        flex={1}
                        maxWidth={'100%'}
                        minWidth={'auto'}
                        wordBreak="break-word">
                        {data?.headings}
                      </L.Txt>
                      <L.Txt
                        flex={1}
                        maxWidth={'100%'}
                        minWidth={'auto'}
                        wordBreak="break-word">
                        {data?.contents}
                      </L.Txt>
                      <L.Txt fontStyle="italic" fontSize={12}>
                        {moment(data?.time).format('HH:mm DD/MM/YYYY')}
                      </L.Txt>
                    </L.Cover>
                  </L.Card>
                </L.Item>
              );
            })}
          </L.List>
        </L.Card>
      )}
    </L.Block>
  );
}
