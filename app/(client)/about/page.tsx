import { Screens } from "@/pattern/screens";
import { L } from "libs/at";

export default function Page() {
  return (
    <L.NoSsr>
      <Screens.About />
    </L.NoSsr>
  );
}
