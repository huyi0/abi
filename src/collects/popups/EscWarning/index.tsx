import { $Popup } from 'libs/es';
import { L } from 'libs/at';
import styles from './styles.module.scss';

export default function Index({ open, onToggle }: $Popup) {
  return (
    <L.Popup
      action={{ onToggle }}
      set={{
        clsPopup: styles.popup,
        clsOverlay: styles.overlay,
        open,
        isCloseOutside: true,
        header: () => {
          return <></>;
        },
        content: () => {
          return <></>;
        },
      }}
    />
  );
}
