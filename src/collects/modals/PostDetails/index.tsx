import { sStoreCtx } from '@/assets/context';

import { $Modal } from 'libs/es';
import { L } from 'libs/at';
import styles from './styles.module.scss';

export default function Index({ open, onToggle, ...args }: $Modal) {
  return (
    <L.Modal
      action={{ onToggle }}
      set={{
        clsModal: styles.modal,
        clsOverlay: styles.overlay,
        open,
        isCloseOutside: true,
        header: (props) => <Header {...props} {...args} />,
        content: () => <Content />,
      }}
    />
  );
}

function Header({ ...props }: any) {
  const { post } = sStoreCtx.use();
  return (
    <L.Section
      shadow
      padding={'1rem'}
      gap={'1rem'}
      horizontal
      alignItems="center"
      justifyContent="space-between"
      background={'lightsteelblue'}
      borderRadius={5}
      className={styles.header}>
      <L.Txt
        textAlign="center"
        minWidth={'unset'}
        flex={1}
        tag="p"
        fontSize={24}
        letterSpacing={1}
        justifyContent="center"
        color="white">
        {post?.title}
      </L.Txt>
      <L.Btn
        className={styles.btnClose}
        width={'fit-content'}
        background={'lightcoral'}
        shadow
        hover
        onClick={props.onToggle}
        color="white">
        X
      </L.Btn>
    </L.Section>
  );
}
const Content = () => {
  const { post } = sStoreCtx.use();
  return (
    <L.Section flex={1} marginTop={'1rem'} className={styles.content} gap={16}>
      <L.Box width={'40%'} className={styles.left}>
        <L.Card height={'100%'}>
          <L.Txt fontStyle="italic">Đang phát triển ...</L.Txt>
        </L.Card>
      </L.Box>
      <L.Box width={'100%'}>
        <L.Cover
          isScrolling
          gap={'0.5rem'}
          flexWrap="wrap"
          dangerouslySetInnerHTML={{ __html: post?.content }}
        />
      </L.Box>
    </L.Section>
  );
};
