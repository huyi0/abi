import * as FileEnv from 'public/js/env.json';
export type $Env = Partial<typeof FileEnv>;
