import { $Modal, TxtField } from 'libs/es';
import { L } from 'libs/at';
import { map } from 'lodash';
import { FormProvider, useForm } from 'react-hook-form';
import styles from './styles.module.scss';

export default function Index({ open, onToggle, ...args }: $Modal) {
  return (
    <L.Modal
      action={{ onToggle }}
      set={{
        clsModal: styles.modal,
        clsOverlay: styles.overlay,
        open,
        isCloseOutside: true,
        header: (props) => {
          return <Header {...props} {...args} />;
        },
        content: () => {
          return <Content />;
        },
      }}
    />
  );
}

const Header = ({ ...props }: any) => {
  return <button onClick={props.onToggle}>X</button>;
};

const Content = () => {
  const methods = useForm({ mode: 'all' });

  return (
    <FormProvider {...methods}>
      <L.Contain flex={1}>
        <L.Box flex={1}>
          <L.AutoComplete
            {...{
              impact({ onToggle }) {
                return (
                  <L.Cover onClick={onToggle}>
                    <TxtField />
                  </L.Cover>
                );
              },
              contents() {
                return (
                  <L.Card
                    background="white"
                    isScrolling
                    flex={1}
                    maxHeight={500}>
                    <L.List column={1}>
                      {map(lst, (item, idx) => {
                        return (
                          <L.Item key={idx} padding={8} hover>
                            {item.CodeName}
                          </L.Item>
                        );
                      })}
                    </L.List>
                  </L.Card>
                );
              },
            }}
          />
        </L.Box>
      </L.Contain>
    </FormProvider>
  );
};

const lst = [
  {
    CodeID: 205,
    Code: '0617',
    CodeName: 'Châm Cứu',
    CodeNameVI: 'Châm Cứu',
    CodeNameEN: 'Châm Cứu',
    RefType: 'INUN',
    OrderNum: 1,
  },
  {
    CodeID: 129,
    Code: '0231',
    CodeName: 'Đơn vị can thiệp mạch máu',
    CodeNameVI: 'Đơn vị can thiệp mạch máu',
    CodeNameEN: 'Đơn vị can thiệp mạch máu',
    RefType: 'INUN',
    OrderNum: 2,
  },
  {
    CodeID: 135,
    Code: '0237',
    CodeName: 'Đơn vị Hồi sức tích cực Covid-19',
    CodeNameVI: 'Đơn vị Hồi sức tích cực Covid-19',
    CodeNameEN: 'Đơn vị Hồi sức tích cực Covid-19',
    RefType: 'INUN',
    OrderNum: 3,
  },
  {
    CodeID: 115,
    Code: '0216',
    CodeName: 'Khoa Bỏng - Tạo hình thẩm mỹ',
    CodeNameVI: 'Khoa Bỏng - Tạo hình thẩm mỹ',
    CodeNameEN: 'Khoa Bỏng - Tạo hình thẩm mỹ',
    RefType: 'INUN',
    OrderNum: 4,
  },
  {
    CodeID: 100,
    Code: '0201',
    CodeName: 'Khoa Cấp cứu',
    CodeNameVI: 'Khoa Cấp cứu',
    CodeNameEN: 'Khoa Cấp cứu',
    RefType: 'INUN',
    OrderNum: 5,
  },
  {
    CodeID: 130,
    Code: '0232',
    CodeName: 'Khoa Chẩn đoán hình ảnh',
    CodeNameVI: 'Khoa Chẩn đoán hình ảnh',
    CodeNameEN: 'Khoa Chẩn đoán hình ảnh',
    RefType: 'INUN',
    OrderNum: 6,
  },
  {
    CodeID: 127,
    Code: '0229',
    CodeName: 'Khoa Chấn thương chỉnh hình',
    CodeNameVI: 'Khoa Chấn thương chỉnh hình',
    CodeNameEN: 'Khoa Chấn thương chỉnh hình',
    RefType: 'INUN',
    OrderNum: 7,
  },
  {
    CodeID: 290,
    Code: '06DA',
    CodeName: 'Khoa Đau 1 (PK)',
    CodeNameVI: 'Khoa Đau 1 (PK)',
    CodeNameEN: 'Khoa Đau 1 (PK)',
    RefType: 'INUN',
    OrderNum: 8,
  },
  {
    CodeID: 265,
    Code: '0677',
    CodeName: 'Khoa Đau 2 (PK)',
    CodeNameVI: 'Khoa Đau 2 (PK)',
    CodeNameEN: 'Khoa Đau 2 (PK)',
    RefType: 'INUN',
    OrderNum: 9,
  },
  {
    CodeID: 266,
    Code: '0678',
    CodeName: 'Khoa Đau 3 (PK)',
    CodeNameVI: 'Khoa Đau 3 (PK)',
    CodeNameEN: 'Khoa Đau 3 (PK)',
    RefType: 'INUN',
    OrderNum: 10,
  },
  {
    CodeID: 267,
    Code: '0679',
    CodeName: 'Khoa Đau 4 (PK)',
    CodeNameVI: 'Khoa Đau 4 (PK)',
    CodeNameEN: 'Khoa Đau 4 (PK)',
    RefType: 'INUN',
    OrderNum: 11,
  },
  {
    CodeID: 117,
    Code: '0218',
    CodeName: 'Khoa Điều trị đau - VLTL - YHCT',
    CodeNameVI: 'Khoa Điều trị đau - VLTL - YHCT',
    CodeNameEN: 'Khoa Điều trị đau - VLTL - YHCT',
    RefType: 'INUN',
    OrderNum: 12,
  },
  {
    CodeID: 116,
    Code: '0217',
    CodeName: 'Khoa Điều trị theo yêu cầu',
    CodeNameVI: 'Khoa Điều trị theo yêu cầu',
    CodeNameEN: 'Khoa Điều trị theo yêu cầu',
    RefType: 'INUN',
    OrderNum: 13,
  },
  {
    CodeID: 126,
    Code: '0228',
    CodeName: 'Khoa Dinh Dưỡng',
    CodeNameVI: 'Khoa Dinh Dưỡng',
    CodeNameEN: 'Khoa Dinh Dưỡng',
    RefType: 'INUN',
    OrderNum: 14,
  },
  {
    CodeID: 119,
    Code: '0221',
    CodeName: 'Khoa ĐTTYC (Lầu 2)',
    CodeNameVI: 'Khoa ĐTTYC (Lầu 2)',
    CodeNameEN: 'Khoa ĐTTYC (Lầu 2)',
    RefType: 'INUN',
    OrderNum: 15,
  },
  {
    CodeID: 118,
    Code: '0220',
    CodeName: 'Khoa ĐTTYC (Lầu 3)',
    CodeNameVI: 'Khoa ĐTTYC (Lầu 3)',
    CodeNameEN: 'Khoa ĐTTYC (Lầu 3)',
    RefType: 'INUN',
    OrderNum: 16,
  },
  {
    CodeID: 124,
    Code: '0226',
    CodeName: 'Khoa Dược (Trực Dược)',
    CodeNameVI: 'Khoa Dược (Trực Dược)',
    CodeNameEN: 'Khoa Dược (Trực Dược)',
    RefType: 'INUN',
    OrderNum: 17,
  },
  {
    CodeID: 122,
    Code: '0224',
    CodeName: 'Khoa Giải phẩu bệnh',
    CodeNameVI: 'Khoa Giải phẩu bệnh',
    CodeNameEN: 'Khoa Giải phẩu bệnh',
    RefType: 'INUN',
    OrderNum: 18,
  },
  {
    CodeID: 138,
    Code: '0240',
    CodeName: 'Khoa Hậu Phẫu',
    CodeNameVI: 'Khoa Hậu Phẫu',
    CodeNameEN: 'Khoa Hậu Phẫu',
    RefType: 'INUN',
    OrderNum: 19,
  },
  {
    CodeID: 101,
    Code: '0202',
    CodeName: 'Khoa Hô hấp',
    CodeNameVI: 'Khoa Hô hấp',
    CodeNameEN: 'Khoa Hô hấp',
    RefType: 'INUN',
    OrderNum: 20,
  },
  {
    CodeID: 102,
    Code: '0203',
    CodeName: 'Khoa Hồi sức tích cực - Chống độc',
    CodeNameVI: 'Khoa Hồi sức tích cực - Chống độc',
    CodeNameEN: 'Khoa Hồi sức tích cực - Chống độc',
    RefType: 'INUN',
    OrderNum: 21,
  },
  {
    CodeID: 107,
    Code: '0208',
    CodeName: 'Khoa Khám bệnh',
    CodeNameVI: 'Khoa Khám bệnh',
    CodeNameEN: 'Khoa Khám bệnh',
    RefType: 'INUN',
    OrderNum: 22,
  },
  {
    CodeID: 123,
    Code: '0225',
    CodeName: 'Khoa Kiểm soát nhiễm khuẩn',
    CodeNameVI: 'Khoa Kiểm soát nhiễm khuẩn',
    CodeNameEN: 'Khoa Kiểm soát nhiễm khuẩn',
    RefType: 'INUN',
    OrderNum: 23,
  },
  {
    CodeID: 120,
    Code: '0222',
    CodeName: 'Khoa Mắt',
    CodeNameVI: 'Khoa Mắt',
    CodeNameEN: 'Khoa Mắt',
    RefType: 'INUN',
    OrderNum: 24,
  },
  {
    CodeID: 128,
    Code: '0230',
    CodeName: 'Khoa Ngoại Lồng ngực - Mạch máu - Thần kinh',
    CodeNameVI: 'Khoa Ngoại Lồng ngực - Mạch máu - Thần kinh',
    CodeNameEN: 'Khoa Ngoại Lồng ngực - Mạch máu - Thần kinh',
    RefType: 'INUN',
    OrderNum: 25,
  },
  {
    CodeID: 105,
    Code: '0206',
    CodeName: 'Khoa Ngoại thận - Tiết niệu',
    CodeNameVI: 'Khoa Ngoại thận - Tiết niệu',
    CodeNameEN: 'Khoa Ngoại thận - Tiết niệu',
    RefType: 'INUN',
    OrderNum: 26,
  },
  {
    CodeID: 104,
    Code: '0205',
    CodeName: 'Khoa Ngoại tổng hợp',
    CodeNameVI: 'Khoa Ngoại tổng hợp',
    CodeNameEN: 'Khoa Ngoại tổng hợp',
    RefType: 'INUN',
    OrderNum: 27,
  },
  {
    CodeID: 106,
    Code: '0207',
    CodeName: 'Khoa Nhiễm',
    CodeNameVI: 'Khoa Nhiễm',
    CodeNameEN: 'Khoa Nhiễm',
    RefType: 'INUN',
    OrderNum: 28,
  },
  {
    CodeID: 113,
    Code: '0214',
    CodeName: 'Khoa Nội tiết - Tổng hợp',
    CodeNameVI: 'Khoa Nội tiết - Tổng hợp',
    CodeNameEN: 'Khoa Nội tiết - Tổng hợp',
    RefType: 'INUN',
    OrderNum: 29,
  },
  {
    CodeID: 108,
    Code: '0209',
    CodeName: 'Khoa Phẩu thuật - GMHS',
    CodeNameVI: 'Khoa Phẩu thuật - GMHS',
    CodeNameEN: 'Khoa Phẩu thuật - GMHS',
    RefType: 'INUN',
    OrderNum: 30,
  },
  {
    CodeID: 139,
    Code: '0241',
    CodeName: 'Khoa Phụ khoa',
    CodeNameVI: 'Khoa Phụ khoa',
    CodeNameEN: 'Khoa Phụ khoa',
    RefType: 'INUN',
    OrderNum: 31,
  },
  {
    CodeID: 109,
    Code: '0210',
    CodeName: 'Khoa Phụ sản',
    CodeNameVI: 'Khoa Phụ sản',
    CodeNameEN: 'Khoa Phụ sản',
    RefType: 'INUN',
    OrderNum: 32,
  },
  {
    CodeID: 140,
    Code: '0242',
    CodeName: 'Khoa Răng Hàm Mặt',
    CodeNameVI: 'Khoa Răng Hàm Mặt',
    CodeNameEN: 'Khoa Răng Hàm Mặt',
    RefType: 'INUN',
    OrderNum: 33,
  },
  {
    CodeID: 134,
    Code: '0236',
    CodeName: 'Khoa Tạo hình Thẩm mỹ',
    CodeNameVI: 'Khoa Tạo hình Thẩm mỹ',
    CodeNameEN: 'Khoa Tạo hình Thẩm mỹ',
    RefType: 'INUN',
    OrderNum: 34,
  },
  {
    CodeID: 125,
    Code: '0227',
    CodeName: 'Khoa Thận - Thận nhân tạo',
    CodeNameVI: 'Khoa Thận - Thận nhân tạo',
    CodeNameEN: 'Khoa Thận - Thận nhân tạo',
    RefType: 'INUN',
    OrderNum: 35,
  },
  {
    CodeID: 110,
    Code: '0211',
    CodeName: 'Khoa Thần kinh',
    CodeNameVI: 'Khoa Thần kinh',
    CodeNameEN: 'Khoa Thần kinh',
    RefType: 'INUN',
    OrderNum: 36,
  },
  {
    CodeID: 111,
    Code: '0212',
    CodeName: 'Khoa Tiêu hóa',
    CodeNameVI: 'Khoa Tiêu hóa',
    CodeNameEN: 'Khoa Tiêu hóa',
    RefType: 'INUN',
    OrderNum: 37,
  },
  {
    CodeID: 112,
    Code: '0213',
    CodeName: 'Khoa Tim mạch',
    CodeNameVI: 'Khoa Tim mạch',
    CodeNameEN: 'Khoa Tim mạch',
    RefType: 'INUN',
    OrderNum: 38,
  },
  {
    CodeID: 114,
    Code: '0215',
    CodeName: 'Khoa Y học dân tộc',
    CodeNameVI: 'Khoa Y học dân tộc',
    CodeNameEN: 'Khoa Y học dân tộc',
    RefType: 'INUN',
    OrderNum: 39,
  },
  {
    CodeID: 136,
    Code: '0238',
    CodeName: 'Khu Điều trị Covid-19',
    CodeNameVI: 'Khu Điều trị Covid-19',
    CodeNameEN: 'Khu Điều trị Covid-19',
    RefType: 'INUN',
    OrderNum: 40,
  },
  {
    CodeID: 137,
    Code: '0239',
    CodeName: 'Khu Điều trị Covid-19_2',
    CodeNameVI: 'Khu Điều trị Covid-19_2',
    CodeNameEN: 'Khu Điều trị Covid-19_2',
    RefType: 'INUN',
    OrderNum: 41,
  },
  {
    CodeID: 206,
    Code: '0618',
    CodeName: 'Vật lý trị liệu',
    CodeNameVI: 'Vật lý trị liệu',
    CodeNameEN: 'Vật lý trị liệu',
    RefType: 'INUN',
    OrderNum: 42,
  },
  {
    CodeID: 294,
    Code: '0701',
    CodeName: 'Xét nghiệm',
    CodeNameVI: 'Xét nghiệm',
    CodeNameEN: 'Xét nghiệm',
    RefType: 'INUN',
    OrderNum: 43,
  },
  {
    CodeID: 224,
    Code: '0635',
    CodeName: 'YHCT 1 (PK)',
    CodeNameVI: 'YHCT 1 (PK)',
    CodeNameEN: 'YHCT 1 (PK)',
    RefType: 'INUN',
    OrderNum: 44,
  },
  {
    CodeID: 268,
    Code: '0680',
    CodeName: 'YHCT 2 (PK)',
    CodeNameVI: 'YHCT 2 (PK)',
    CodeNameEN: 'YHCT 2 (PK)',
    RefType: 'INUN',
    OrderNum: 45,
  },
];
