import { ctrl } from '@/process/ctrl';
import { StateCreator } from 'zustand';

const init = {
  posts: [],
  post: {},
};

type $Post = {
  [T: string]: string;
};

export type State = {
  onPosts: () => Promise<void>;
  posts: $Post[];
  post: $Post;
  pickPost(post: $Post): void;
};

export const postSlice: StateCreator<State> = (set) => {
  return {
    ...init,
    async onPosts() {
      const rs: any = await ctrl().getPosts();
      set({
        posts: rs?.items,
      });
    },
    pickPost(post: $Post) {
      set({ post });
    },
  } as const;
};
