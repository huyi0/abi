'use client';
import { ModalCtx, sStoreCtx } from '@/assets/context';
import { $Media } from '@/assets/types/media';
import { ctrl } from '@/process/ctrl';
import { L } from 'libs/at';
import { filter, map } from 'lodash';
import { useEffect } from 'react';
import { toast } from 'react-toastify';
import styles from './styles.module.scss';

export default function Index() {
  const { onMedias, medias, media_url } = sStoreCtx.use();
  const { onToggle } = ModalCtx.use();

  useEffect(() => {
    onMedias();
  }, []);

  const column = 3;

  const dimension = Array.from({ length: column }, (_, colIndex) =>
    filter(medias, (_, index) => index % column === colIndex),
  );

  const onDelete = async (item: $Media) => {
    try {
      if (item?.Key) {
        const rs = await ctrl().onDeleteFile({ Key: item?.Key });
        if (rs.status) {
          toast.success('Xoá file thành công');
        }
      }
    } catch (error) {
      console.info('error :>> ', error);
    } finally {
      onMedias();
    }
  };

  return (
    <L.Contain vertical className={styles.about}>
      <L.Yard className={styles.yard} flex={1} vertical>
        <L.Txt fontStyle="italic">Đang phát triễn ...</L.Txt>
        <L.Cover gap={16}>
          <L.Btn
            kindType="outline"
            onClick={() => {
              onToggle({ key: 'Upload' });
            }}>
            Upload
          </L.Btn>
          <L.Btn kindType="outline" onClick={onMedias}>
            Refresh
          </L.Btn>
        </L.Cover>

        <L.Block gap={16} overflow="auto" margin={'-1rem'} flex={1}>
          <L.List
            padding={'1rem'}
            column={3}
            backgroundColor="transparent"
            isScrolling
            flex={1}>
            {map(dimension, (slice, idx) => {
              return (
                <L.Item key={idx} vertical gap={16}>
                  {map(slice, (item, index) => {
                    return (
                      <L.Card key={index} flex={0} aspectRatio={1}>
                        <L.Cover onClick={() => onDelete(item)}>
                          <L.Btn
                            borderColor="lightcoral"
                            color="lightcoral"
                            kindType="outline">
                            Del
                          </L.Btn>
                        </L.Cover>
                        <picture style={{ aspectRatio: 1 }}>
                          <img src={media_url + '/' + item?.Key} alt="" />
                        </picture>
                      </L.Card>
                    );
                  })}
                </L.Item>
              );
            })}
          </L.List>
        </L.Block>
      </L.Yard>
    </L.Contain>
  );
}
