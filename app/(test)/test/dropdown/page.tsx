'use client'
import TrackPosition from '@/pattern/test/Track';
import { L } from 'libs/at';
import React from 'react';

export default function Index() {
  return (
    <L.Contain vertical>
      <L.Box minHeight={'100vh'} />

      <L.Dropbox
        {...{
          impact({ onToggle }) {
            return (
              <L.Box onClick={onToggle}>
                <L.Txt>Helo</L.Txt>
              </L.Box>
            );
          },
          contents() {
            return (
              <L.Card>
                <L.Txt>Helo</L.Txt>
              </L.Card>
            );
          },
        }}
      />

      <TrackPosition/>

      <L.Box minHeight={'100vh'} />
    </L.Contain>
  );
}
