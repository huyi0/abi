'use client';
import { initializeApp } from 'firebase/app';
import { PropsWithChildren, createContext, useContext } from 'react';

const useLogic = () => {
    const firebaseConfig = {
        apiKey: 'AIzaSyDbCAXfTWrKAmKS_OsHi8RyAicAacn0VI4',
        authDomain: 'huyi-24a51.firebaseapp.com',
        projectId: 'huyi-24a51',
        storageBucket: 'huyi-24a51.appspot.com',
        messagingSenderId: '468443919890',
        appId: '1:468443919890:web:ce1eb3dfbcce2f6a550acb',
        measurementId: 'G-KMJX65SJ9L',
    };

    initializeApp(firebaseConfig);

    return {};
};


type ValueCtx = ReturnType<typeof useLogic>;

export const FirebaseCtx = createContext({} as ValueCtx);
export const useFirebaseCtx = () => useContext(FirebaseCtx);

export const FirebaseProvider = ({ ...props }: PropsWithChildren) => {
  const valueCtx = useLogic();
  return (
    <FirebaseCtx.Provider value={{ ...valueCtx, ...props }}>
      <>{props.children}</>
    </FirebaseCtx.Provider>
  );
};
