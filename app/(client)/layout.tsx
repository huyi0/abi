'use client';
import { Cores } from '@/pattern/cores';
import { PropsWithChildren } from 'react';

export default function Layout({ children }: PropsWithChildren) {
  return (
    <Cores.Layout>
      <Cores.Header />
      <Cores.Main> {children}</Cores.Main>
      <Cores.Footer />
      <Cores.Dev />
    </Cores.Layout>
  );
}
