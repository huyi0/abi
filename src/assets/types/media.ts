export type $Media = {
  Key?: string;
  LastModified?: string;
  ETag?: string;
  Size?: number;
  StorageClass?: string;
  Owner?: IOwner;
} & { [T: string]: string };

export interface IOwner {
  DisplayName?: string;
  ID?: string;
}
