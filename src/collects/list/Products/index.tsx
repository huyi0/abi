'use client';
import { ModalCtx, sStoreCtx } from '@/assets/context';
import { L } from 'libs/at';
import { map } from 'lodash';
import { useEffect } from 'react';
import styles from './styles.module.scss';

export default function Index() {
  const { posts, onPosts, pickPost } = sStoreCtx.use();
  const { onToggle } = ModalCtx.use();

  useEffect(() => {
    onPosts();
  }, []);

  return (
    <>
      <L.List isScrolling column={1} className={styles.List}>
        {map(posts, (item, idx) => {
          return (
            <L.Item key={idx} className={styles.Item}>
              <L.Card
                flex={1}
                shadow
                hover
                vertical
                className={styles.Card}
                onClick={() => {
                  onToggle({ key: 'PostDetails' });
                  pickPost(item);
                }}>
                <L.Cover aspectRatio={0}>
                  <L.Txt
                    letterSpacing={1}
                    justifyContent="center"
                    alignItems="center">
                    {item?.pubDate}
                  </L.Txt>
                </L.Cover>
                <L.Txt
                  minWidth={'unset'}
                  dangerouslySetInnerHTML={{
                    __html: item?.title,
                  }}></L.Txt>
              </L.Card>
            </L.Item>
          );
        })}
      </L.List>
    </>
  );
}
