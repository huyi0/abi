'use client';
import { ModalCtx, TestCtx } from '@/assets/context';
import { L } from 'libs/at';

export default function Page() {
  const { onToggle } = ModalCtx.use();

  return (
    <>
      <L.Btn
        kindType="small"
        onClick={() => {
          onToggle({ key: 'Demo' });
        }}>
        modal
      </L.Btn>
      <TestCtx.Provider>
        <TestCtx.Consumer>
          {() => {
            return (
              <L.Contain>
                <L.Card>
                  <p>helo dcd</p>
                </L.Card>
              </L.Contain>
            );
          }}
        </TestCtx.Consumer>
      </TestCtx.Provider>
    </>
  );
}
