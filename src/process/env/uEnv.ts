import { readFileSync } from 'fs';
import { $Env } from './tEnv';

const uEnv = (): $Env | null => {
  try {
    const e = readFileSync('public/js/env.json', 'utf-8');
    return JSON.parse(e);
  } catch (error) {
    // console.info('error :>> ', error);
    return null;
  }
};

export default uEnv();
