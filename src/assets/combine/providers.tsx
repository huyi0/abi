'use client';
import {
  AppCtx,
  ModalCtx,
  OneSignalCtx,
  PopupCtx,
  sStoreCtx,
} from '@/assets/context';
import { ComponentType, ReactNode } from 'react';

type ProviderType = ComponentType<{ children: ReactNode }>;

type CombinedProviderProps = {
  children: ReactNode;
};

function combineProviders(
  providers: ProviderType[],
): ComponentType<CombinedProviderProps> {
  return function CombinedProvider(props: CombinedProviderProps) {
    return providers.reduceRight((children, Provider) => {
      return <Provider>{children}</Provider>;
    }, props.children);
  };
}

export const CombinedProviders = combineProviders([
  sStoreCtx.Provider,
  AppCtx.Provider,
  OneSignalCtx.Provider,
  PopupCtx.Provider,
  ModalCtx.Provider,
]);
