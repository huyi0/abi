import { L } from 'libs/at';
import styles from "./styles.module.scss";

export default function Index({ children }: any) {
  return (
    <L.Core tag="main" shadow className={styles.Main}>
      {children}
    </L.Core>
  );
}
