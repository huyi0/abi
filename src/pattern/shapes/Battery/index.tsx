'use client';
import { L } from 'libs/at';
import { memo, useEffect, useState } from 'react';

declare global {
  interface Navigator {
    getBattery(): Promise<BatteryManager>;
  }

  interface BatteryManager extends EventTarget {
    charging: boolean;
    chargingTime: number;
    dischargingTime: number;
    level: number;
    onchargingchange: ((this: BatteryManager, ev: Event) => any) | null;
    onchargingtimechange: ((this: BatteryManager, ev: Event) => any) | null;
    ondischargingtimechange: ((this: BatteryManager, ev: Event) => any) | null;
    onlevelchange: ((this: BatteryManager, ev: Event) => any) | null;
  }
}

type Init = Partial<
  Pick<
    BatteryManager,
    'charging' | 'chargingTime' | 'dischargingTime' | 'level'
  >
>;

export default memo(function Battery({ size = '2rem' }: { size?: string }) {
  const [pin, setPin] = useState({} as Init);

  useEffect(() => {
    function updatePin(battery: any) {
      setPin({
        level: Number(battery?.level) * 100,
        charging: battery?.charging,
      });
    }
    
    async function checkBatteryAPIAndSetup() {
      try {
        const battery = await navigator.getBattery();
        battery.addEventListener('chargingchange', updatePin);
        battery.addEventListener('levelchange', updatePin);
        battery.addEventListener('chargingtimechange', updatePin);
        battery.addEventListener('dischargingtimechange', updatePin);

        updatePin(battery);
        return () => {
          battery.removeEventListener('chargingchange', updatePin);
          battery.removeEventListener('levelchange', updatePin);
          battery.removeEventListener('chargingtimechange', updatePin);
          battery.removeEventListener('dischargingtimechange', updatePin);
        };
      } catch (error) {
        console.error('Battery status is not supported.');
        setPin((prev) => ({ ...prev, supported: false }));
      }
    }

    checkBatteryAPIAndSetup();
  }, []);

  const level = Number(pin.level || 0);

  return (
    <>
      <L.Group
        shadow
        isBorder
        borderColor={pin.charging ? 'lightgreen' : 'lightskyblue'}
        width={`calc(${size})`}
        height={`calc(${size}/2)`}
        overflow="hidden"
        backgroundColor={'white'}
        borderRadius={'2rem'}>
        <L.Cover
          width={`calc(${level}%)`}
          height={'100%'}
          position="absolute"
          backgroundColor={
            pin.charging
              ? 'lightgreen'
              : level > 20
                ? 'lightskyblue'
                : 'lightcoral'
          }
        />
        <L.Txt
          fontSize={`calc(${size}/6)`}
          width={'100%'}
          height={'100%'}
          alignItems="center"
          justifyContent="center"
          position="absolute">{`${level?.toFixed(0)}%`}</L.Txt>
      </L.Group>
    </>
  );
});
