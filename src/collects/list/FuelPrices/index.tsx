'use client';
import { L } from 'libs/at';
import { ctrl } from '@/process/ctrl';
import { map } from 'lodash';
import React from 'react';
import styles from './styles.module.scss';

export default function Index() {
  const [fules, setFules] = React.useState([] as any[]);


  const onGetList = async () => {
    const rs: any = await ctrl().onGetFuelPrices();
    setFules(rs);
  };

  React.useEffect(() => {
    onGetList();
  }, []);

  return (
    <L.Legend
      label={<L.Txt color="lightblue">Giá xăng dầu</L.Txt>}
      shadow
      padding={'1rem'}
      className={styles.block}
      borderColor="lightgray">
      <L.List column={1}>
        {map(fules, (itm, idx) => {
          return (
            <L.Item key={idx}>
              <L.Cover
                flex={1}
                width={'100%'}
                gap={'1rem'}
                justifyContent="space-between">
                <L.Txt justifyContent="flex-start" flex={1}>
                  {itm?.item}
                </L.Txt>
                <L.Txt justifyContent="center" flex={1}>
                  {itm?.price}
                </L.Txt>
                <L.Txt justifyContent="center" flex={1}>
                  {itm?.change}
                </L.Txt>
              </L.Cover>
            </L.Item>
          );
        })}
      </L.List>
    </L.Legend>
  );
}
