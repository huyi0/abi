import { L } from 'libs/at';
import { $Notis } from '@/assets/types/noti';

export default function first({ item }: { item: $Notis }) {
  return (
    <L.Card shadow={false} padding={0}>
      <L.Box aspectRatio={1}>
        <picture>
          <img src={item?.icon} alt="" />
        </picture>
      </L.Box>
      <L.Cover vertical>
        <L.Txt
          flex={1}
          maxWidth={'100%'}
          minWidth={'auto'}
          wordBreak="break-word"
          fontWeight={600}
          fontSize={14}>
          {item?.title}
        </L.Txt>
        <L.Txt
          flex={1}
          maxWidth={'100%'}
          minWidth={'auto'}
          wordBreak="break-word"
          fontSize={13}>
          {item?.body}
        </L.Txt>
      </L.Cover>
    </L.Card>
  );
}
