import { writeFileSync } from 'fs';

(() => {
  const defineEnv = {
    BE_URL: process.env.BE_URL,
    CORS_URL: process.env.CORS_URL,
    LOGO: process.env.LOGO,
  };

  const envDta = JSON.stringify(defineEnv, undefined, 2);
  writeFileSync('public/js/env.json', envDta);
})();
