'use client';
import { L } from 'libs/at';
import styles from './styles.module.scss';
import Countdown from '@/pattern/parts/Countdown';

export default function Index() {
  return (
    <L.Contain vertical className={styles.about}>
      <L.Yard className={styles.yard} vertical>
        <L.Txt fontStyle="italic">Đang phát triễn ...</L.Txt>

        <L.Block gap={16} flexDirection="column">
          <Countdown />
          <L.Calendar />
        </L.Block>
      </L.Yard>
    </L.Contain>
  );
}
