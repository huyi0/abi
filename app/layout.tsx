import { CombinedProviders } from '@/assets/combine/providers';
import { args } from '@/init';
import axios from 'axios';
import { address } from 'ip';
import type { Metadata } from 'next';
import { headers } from 'next/headers';
import { PropsWithChildren } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './globals.scss';
import { ArgsCtx } from '@/assets/context';

export default async function Layout({ children }: PropsWithChildren) {
  const env = await onGetEnv();
  args.env = env;

  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta content="IE=edge" />
        <meta name="theme-color" content="#00b5f1" />
        <meta name="viewport" content="viewport-fit=cover" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"
        />
        <link rel="manifest" href="/manifest.json" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin=""
        />

        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"
          integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA=="
          crossOrigin="anonymous"
          referrerPolicy="no-referrer"
        />

        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/fontawesome.min.css"
          integrity="sha512-d0olNN35C6VLiulAobxYHZiXJmq+vl+BGIgAxQtD5+kqudro/xNMvv2yIHAciGHpExsIbKX3iLg+0B6d0k4+ZA=="
          crossOrigin="anonymous"
          referrerPolicy="no-referrer"
        />
      </head>
      <body className="light" data-ip={address()}>
        <ArgsCtx.Provider {...{ env }}>
          <CombinedProviders>{children}</CombinedProviders>
          <ToastContainer />
        </ArgsCtx.Provider>
      </body>
    </html>
  );
}

export const revalidate = 360;

export const metadata: Metadata = {
  title: 'demo',
  description: 'demo',
};

const onGetEnv = async () => {
  const header = headers();
  const curl = header.get('x-url-origin');

  try {
    const { data } = await axios(curl + `/js/env.json`);
    return data;
  } catch (error) {
    console.info('error :>> ', error);
    return null;
  }
};
