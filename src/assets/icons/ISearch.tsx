import React from 'react';

export default function Icon({ ...props }) {
  return React.cloneElement(
    <svg
      width={24}
      height={24}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path
        d="M19.1998 19.2L15.0424 15.0426M15.0424 15.0426C16.1676 13.9174 16.7997 12.3912 16.7997 10.8C16.7997 9.20866 16.1676 7.68255 15.0424 6.55734C13.9172 5.43213 12.3911 4.79999 10.7998 4.79999C9.20848 4.79999 7.68237 5.43213 6.55715 6.55734C5.43194 7.68255 4.7998 9.20866 4.7998 10.8C4.7998 12.3912 5.43194 13.9174 6.55715 15.0426C7.68237 16.1678 9.20848 16.7999 10.7998 16.7999C12.3911 16.7999 13.9172 16.1678 15.0424 15.0426Z"
        stroke={props.stroke || '#52C0B0'}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>,
    props,
  );
}
