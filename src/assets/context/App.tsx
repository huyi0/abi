'use client';
import { onCheckVersion } from '@/process/ctrl';
import { toString } from 'lodash';
import { useEffect } from 'react';
import { onGumCtx } from './GumCtx';
import { sStoreCtx } from '.';

export const { Provider, Consumer, use } = onGumCtx<
  ReturnType<typeof useLogic>
>({
  useLogic,
});

function useLogic() {
  const { mode } = sStoreCtx.use();

  useEffect(() => {
    const body = document.body;
    body.className = toString(mode);
  }, [mode]);

  useEffect(() => {
    onCheckVersion();
  }, []);

  return {};
}
