import Demo from './Demo';
import Dev from './Dev';
import Ejson from './Ejson';
import PostDetails from './PostDetails';
import Test from './Test';
import TestD from './TestD';
import Trailer from './Trailer';
import Upload from './Upload';

export type $ModalKeys = keyof ReturnType<typeof ModalExports>;
export function ModalExports() {
  return {
    Dev,
    Demo,
    PostDetails,
    Ejson,
    Test,
    Trailer,
    TestD,
    Upload,
  };
}
