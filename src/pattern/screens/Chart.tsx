'use client';
import * as echarts from 'echarts';
import { L } from 'libs/at';
import { Master } from 'libs/es';
import { useEffect } from 'react';

export default function Page() {
  useEffect(() => {
    const chartDom = document.getElementById('do');

    if (chartDom) {
      const myChart = echarts.init(chartDom);
      myChart.setOption(option);
      myChart.resize({
        width: 'auto',
        height: 'auto',
      });
    }
  }, [option]);

  return (
    <L.Contain flex={1}>
      <L.Block flex={1}>
        <L.Cover isScrolling flex={1}>
          <Master
            id="do"
            style={{
              width: 900,
              height: 700,
            }}
          />
        </L.Cover>
      </L.Block>
    </L.Contain>
  );
}

const colors = ['#5470C6', '#91CC75', '#EE6666'];
const option: echarts.EChartsOption = {
  color: colors,
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'cross',
    },
  },
  grid: {
    left: '20%',
    bottom: '30%',
  },
  toolbox: {
    feature: {
      dataView: { show: true, readOnly: false },
      restore: { show: true },
      saveAsImage: { show: true },
    },
  },

  xAxis: [
    {
      type: 'category',
      position: 'top',
      data: ['9:30', '10:20', '11:10', '13:05'],
    },
    {
      name: 'SpO2 (%)',
      offset: 0,
      axisLine: {
        show: false,
      },
      position: 'bottom',
      data: ['97', '99', '99', '96'],
    },
    {
      name: 'Thở Oxi',
      offset: 40,
      axisLine: {
        show: false,
      },
      position: 'bottom',
      data: ['Không', 'Không', 'Có', 'Không'],
    },
    {
      name: 'Tri giác (TNĐM)',
      offset: 80,
      position: 'bottom',
      axisLine: {
        show: false,
      },
      data: ['1', '2', '3', '4'],
    },
    {
      name: 'Tổng điểm MEWS',
      offset: 120,
      position: 'bottom',
      axisLine: {
        show: false,
      },
      data: ['1.1', '2', '3', '4'],
    },
    {
      name: 'Cân nặng (Kg)',
      offset: 160,
      position: 'bottom',
      axisLine: {
        show: false,
      },
      data: ['1.1', '2', '3', '4'],
    },
  ],
  yAxis: [
    {
      type: 'value',
      name: 'Huyết áp \n mạch',
      position: 'left',
      alignTicks: true,
      offset: 80,
      axisLine: {
        show: false,
        lineStyle: {
          color: colors[0],
        },
      },
    },
    {
      type: 'value',
      name: 'Nhịp \n thở',
      position: 'left',
      alignTicks: true,
      offset: 40,
      axisLine: {
        show: false,
        lineStyle: {
          color: colors[1],
        },
      },
    },
    {
      type: 'value',
      name: 'Nhiệt\nđộ',
      offset: 0,
      position: 'left',
      alignTicks: true,
      axisLine: {
        show: false,
        lineStyle: {
          color: colors[2],
        },
      },
    },
  ],
  series: [
    {
      type: 'line',
      yAxisIndex: 0,
      data: [
        2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3,
      ],
    },
    {
      type: 'line',
      yAxisIndex: 1,
      data: [
        2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3,
      ],
    },
    {
      type: 'line',
      yAxisIndex: 2,
      data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2],
    },
  ],
};
