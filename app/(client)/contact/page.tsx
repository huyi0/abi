import { L } from 'libs/at';
import { Screens } from '@/pattern/screens';

export default function Page() {
  return (
    <L.NoSsr>
      <Screens.Contact />
    </L.NoSsr>
  );
}
