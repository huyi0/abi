export type $Notification = Partial<{
  headings: string;
  contents: string;
  subtitle: string;
  data: object;
}>;

export type $Notis = {
  notificationId: string;
  title?: string;
  body: string;
  icon?: string;
  badgeIcon?: string;
  image?: string;
  actionButtons?: Array<object>;
  topic?: string;
  additionalData?: object;
  launchURL?: string;
  confirmDelivery: boolean;
};
