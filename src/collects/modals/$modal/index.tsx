import { $Modal } from 'libs/es';
import { L } from 'libs/at';
import styles from './styles.module.scss';

export default function Index({ open, onToggle, ...args }: $Modal) {
  return (
    <L.Modal
      action={{ onToggle }}
      set={{
        clsModal: styles.modal,
        clsOverlay: styles.overlay,
        open,
        isCloseOutside: true,
        header: (props) => {
          return <Header {...props} {...args} />;
        },
        content: () => {
          return <Content />;
        },
      }}
    />
  );
}

const Header = ({ ...props }: any) => {
  return <button onClick={props.onToggle}>X</button>;
};

const Content = () => {
  return (
    <>
      <h2>Modal Content</h2>
    </>
  );
};
