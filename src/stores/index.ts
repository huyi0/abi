import { reduce } from 'lodash';
import { persistNSync } from 'persist-and-sync';
import { create } from 'zustand';
import { devtools } from 'zustand/middleware';
import { authSlice } from './auth';
import { blogSlice } from './blog';
import { cinemaSlice } from './cinema';
import { jsonSlice } from './ejson';
import { envSlice } from './env';
import { mediaSlice } from './media';
import { notiSlice } from './noti';
import { postSlice } from './post';
import { settingSlice } from './setting';
import { todoSlice } from './todo';

const sliceFunctions = [
  jsonSlice,
  postSlice,
  todoSlice,
  blogSlice,
  notiSlice,
  cinemaSlice,
  envSlice,
  settingSlice,
  mediaSlice,
  authSlice,
];

type UnionToIntersection<U> = (U extends any ? (k: U) => void : never) extends (
  k: infer I,
) => void
  ? I
  : never;

type State = UnionToIntersection<ReturnType<(typeof sliceFunctions)[number]>>;

export const sStore = create<State>()(
  devtools(
    persistNSync(
      (...a) => {
        return reduce(
          sliceFunctions,
          (prev, cur) => {
            return {
              ...prev,
              ...cur(...a),
            };
          },
          {} as State,
        );
      },
      {
        name: 'store',
      },
    ),
  ),
);
