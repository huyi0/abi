'use client';
import { L } from 'libs/at';

import { sStoreCtx } from '@/assets/context';

import { FormProvider, useForm } from 'react-hook-form';
import styles from './styles.module.scss';

export default function Index() {
  const methods = useForm({ mode: 'all' });
  const { handleSubmit } = methods;
  const { onSendNotification } = sStoreCtx.use();

  const onSend = async (data: object) => {
    await onSendNotification(data);
  };

  return (
    <L.Contain vertical className={styles.about}>
      <L.Yard>
        <L.Card maxWidth={'fit-content'}>
          <FormProvider {...methods}>
            <L.TxtField name="headings" label={'headings'} kind="textarea" />
            <L.TxtField name="contents" label={'contents'} kind="textarea" />

            <L.Cover>
              <L.Btn kindType="shape" onClick={handleSubmit(onSend)}>
                Send
              </L.Btn>
            </L.Cover>
          </FormProvider>
        </L.Card>
      </L.Yard>
      <L.Yard></L.Yard>
    </L.Contain>
  );
}
