'use client';
import cx from 'classnames';
import { L } from 'libs/at';
import { keys, map, padStart } from 'lodash';
import moment from 'moment';
import { useCallback, useEffect, useState } from 'react';
import styles from './styles.module.scss';

export default function Index() {
  return (
    <L.NoSsr>
      <L.Core tag="footer" className={styles.Footer}>
        <L.Card overflow="auto" flex={1}>
          <L.Dropbox
            isOutlet
            impact={({ onToggle }) => {
              return (
                <L.Cover className={styles.RealTime}>
                  <L.Txt hover className={cx(styles.time)} onClick={onToggle}>
                    <L.Icon
                      className={cx(styles.lock, styles.time, 'far fa-clock')}
                    />
                  </L.Txt>
                </L.Cover>
              );
            }}
            contents={() => {
              return (
                <>
                  <L.Calendar />
                </>
              );
            }}
          />

          <Clock />

          <L.Box padding={0} className={styles.txt} justifyContent="center">
            <L.Cover gap={16} alignItems="center">
              <L.Txt className={styles.txtFollow}>Follow Me</L.Txt>
              <hr />
            </L.Cover>
          </L.Box>
          <L.Box
            padding={0}
            className={styles.txt}
            justifyContent="center"></L.Box>
          <L.Box vertical alignItems="center" padding={8} gap={8}>
            <L.Btn shadow hover className={cx(styles.btn, styles.btnPrev)}>
              <span>&#9650;</span>
            </L.Btn>
            <L.Btn shadow hover className={cx(styles.btn, styles.btnNext)}>
              <span>&#9660;</span>
            </L.Btn>
          </L.Box>
        </L.Card>
      </L.Core>
    </L.NoSsr>
  );
}

const useTime = () => {
  const [live, setLive] = useState({
    hh: '00',
    mm: '00',
    ss: '00',
  });

  const updateLive = useCallback(() => {
    const current = moment();
    setLive({
      hh: padStart(current.hours().toString(), 2, '0'),
      mm: padStart(current.minutes().toString(), 2, '0'),
      ss: padStart(current.seconds().toString(), 2, '0'),
    });
  }, []);

  useEffect(() => {
    const interval = setInterval(updateLive, 360);
    return () => clearInterval(interval);
  }, [updateLive]);

  return { live };
};

const Clock = () => {
  const { live } = useTime();

  return (
    <L.Cover
      width={'100%'}
      gap={16}
      alignItems="center"
      className={styles.RealTime}
      vertical>
      {map(keys(live), (item: keyof typeof live) => {
        return (
          <L.Txt width={'100%'} key={item} shadow className={cx(styles.time)}>
            {live[item]}
          </L.Txt>
        );
      })}
    </L.Cover>
  );
};
