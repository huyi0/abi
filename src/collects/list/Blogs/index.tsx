'use client';
import { sStoreCtx } from '@/assets/context';

import { _get, useGetSize } from 'libs/es';
import { L } from 'libs/at';
import { map } from 'lodash';
import moment from 'moment';
import { cache, use } from 'react';
import styles from './styles.module.scss';

export default function Index() {
  const { onBlogs, blogs } = sStoreCtx.use();

  use(cache(onBlogs)());

  const column = _get.Breakpoint({
    breakpoint: {
      1400: 3,
      700: 2,
      500: 1,
    },
    listRect: useGetSize({ Id: 'list' }),
  });

  return (
    <L.Block flex={1} className={styles.blockBlogs}>
      <L.Cover isScrolling margin={-16}>
        <L.List column={column} padding={16} id="list">
          {map(blogs, (item, idx) => {
            return (
              <L.Item key={idx}>
                <L.Card
                  height={'auto'}
                  hover
                  onClick={() => window.open(item.url)}>
                  <L.Box aspectRatio={16 / 9}>
                    <picture>
                      <img src={item.thumbnail_url} alt="" />
                    </picture>
                  </L.Box>
                  <L.Cover vertical gap={16}>
                    <L.Txt fontSize={12} fontStyle="italic">
                      {moment(item?.published_at).format('LLL')}
                    </L.Txt>
                    <L.Txt minWidth={'unset'} fontWeight={600} fontSize={16}>
                      {item?.title}
                    </L.Txt>

                    <L.Txt minWidth={'unset'} fontSize={13}>
                      {item?.contents_short}
                    </L.Txt>
                  </L.Cover>
                </L.Card>
              </L.Item>
            );
          })}
        </L.List>
      </L.Cover>
    </L.Block>
  );
}
