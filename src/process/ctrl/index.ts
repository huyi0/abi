import { $Notification } from '@/assets/types/noti';
import { args, VERSION } from '@/init';
import { api } from '@/process/instance';
import axios from 'axios';
import { eq } from 'lodash';
import { toast } from 'react-toastify';
import parser from 'rss-to-json';
import { parseStringPromise } from 'xml2js';

export const onCheckVersion = () => {
  let flat = true;
  const version_storage = localStorage.getItem('version');
  const domain_storage = localStorage.getItem('domain');
  const domain_browser = location.origin;
  const isNewVersion =
    !eq(version_storage, VERSION) || !eq(domain_storage, domain_browser);

  if (!version_storage || !domain_storage) {
    console.info('set version :>> ', VERSION);
  } else {
    if (isNewVersion) {
      console.info('clear version :>> ', version_storage);
      localStorage.clear();
    } else {
      console.info('old version :>> ', version_storage);
      flat = false;
    }
  }

  if (flat) {
    localStorage.setItem('version', String(VERSION));
    localStorage.setItem('domain', domain_browser);
  }
};

export const ctrl = () => {
  const { env } = args;

  return {
    async getWeather() {
      try {
        //
      } catch (error) {
        console.error('Error fetching weather data:', error);
      }
    },
    async onGetFuelPrices() {
      try {
        const response = await axios.get(
          `${env?.CORS_URL}/https://vnexpress.net/chu-de/gia-xang-dau-3026`,
        );
        const parser = new DOMParser();
        const htmlDoc = parser.parseFromString(response.data, 'text/html');

        const rows = htmlDoc.querySelectorAll('.taxonomy_seo table tbody tr');
        const fuelPriceData = Array.from(rows).map((row) => ({
          item: row.querySelector('td:nth-child(1)')?.textContent?.trim(),
          price: row.querySelector('td:nth-child(2)')?.textContent?.trim(),
          change: row.querySelector('td:nth-child(3)')?.textContent?.trim(),
        }));

        return fuelPriceData;
      } catch (error) {
        console.error('Error fetching gold prices:', error);
      }
    },
    async onGetGoldPrice() {
      try {
        const response = await axios.get(
          `${env?.CORS_URL}/https://sjc.com.vn/xml/tygiavang.xml`,
        );
        const xmlData = response.data;

        // Convert XML to JSON
        const rs = await parseStringPromise(xmlData);
        return rs?.root;
      } catch (error) {
        console.error('Error fetching gold prices:', error);
      }
    },
    async getBlogs() {
      try {
        const rs = await axios.get(
          `${env?.CORS_URL}/https://viblo.asia/api/users/huyi98/posts`,
        );
        return rs.data;
      } catch (error) {
        console.info('error :>> ', error);
        return {};
      }
    },
    async getPosts() {
      try {
        const freed = await parser(
          `${env?.CORS_URL}/https://trainghiemso.vn/feed/`,
        );

        return freed;
      } catch (error) {
        console.info('error :>> ', error);
        return {};
      }
    },
    async getJson() {
      try {
        const rs = await api.get('/v1/json/detail', {
          params: {
            name: 'db',
          },
        });
        return rs.data;
      } catch (error) {
        console.info('error :>> ', error);
        return {};
      }
    },
    async onJsonUpdate({
      Data,
      cb,
    }: {
      Data: object;
      cb?: () => Promise<void>;
    }) {
      try {
        await api.patch('/v1/json/update', {
          name: 'db',
          value: Data,
        });
        await cb?.();
      } catch (error) {
        console.info('error', error);
      }
    },
    async onSyncGit() {
      try {
        await api.get(`${window.location.origin}/api/git`);
        toast('Success git', { type: 'success' });
      } catch (error) {
        toast('Failure git', { type: 'error' });
        console.info('error git: ', error);
      }
    },
    async onGetNotifications() {
      try {
        const rs = await api.get('/v1/notification/list', {});
        return rs.data;
      } catch (error) {
        return {};
      }
    },
    async onSendNotification(payload: $Notification) {
      try {
        const rs = await api.post('/v1/notification/send', payload);
        return rs.data;
      } catch (error) {
        return {};
      }
    },
    async onUploadFile(formdata: any) {
      try {
        const rs = await api.post('/v1/upload/file', formdata, {
          maxBodyLength: Infinity,
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        });
        return rs.data;
      } catch (error) {
        return {};
      }
    },
    async onGetListFile({ Bucket = 'abi' }: { Bucket?: string }) {
      try {
        const rs = await api.get('/v1/upload/list', {
          params: {
            Bucket,
          },
        });
        return rs.data;
      } catch (error) {
        return {};
      }
    },
    async onDeleteFile({ Key }: { Key: string }) {
      try {
        const rs = await api.post('/v1/upload/delete', {
          Bucket: 'abi',
          Key,
        });
        return rs.data;
      } catch (error) {
        return {};
      }
    },
  };
};
