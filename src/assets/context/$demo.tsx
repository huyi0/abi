'use client';
import React, { createContext } from 'react';
import { useForm } from 'react-hook-form';

const useLogic = () => {
  const method = useForm({
    mode: 'onChange',
    defaultValues: {},
  });

  return {
    a: 'helo',
    method,
  };
};

type ArgsCtx = ReturnType<typeof useLogic>;

const Gum = {
  Ctx: createContext({} as ArgsCtx),
  use() {
    return React.use(Gum.Ctx);
  },
  Provider({ ...props }: { children: React.ReactNode }) {
    const returnCtx = useLogic();
    return (
      <Gum.Ctx.Provider value={{ ...returnCtx, ...props }}>
        {props?.children}
      </Gum.Ctx.Provider>
    );
  },
  Consumer({ ...props }: { children(args: ArgsCtx): React.ReactNode }) {
    return (
      <Gum.Ctx.Consumer>
        {(args) => {
          return props?.children?.(args);
        }}
      </Gum.Ctx.Consumer>
    );
  },
};

export const CtxDemo = Gum;
