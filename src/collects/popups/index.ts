import EscWarning from './EscWarning';
export type $PopupKeys = keyof ReturnType<typeof PopupExports>;
export function PopupExports() {
  return {
    EscWarning,
  };
}
