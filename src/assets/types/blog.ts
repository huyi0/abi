export interface $Blog {
  id?: number;
  title?: string;
  slug?: string;
  url?: string;
  user_id?: number;
  transliterated?: string;
  contents_short?: string;
  published_at?: string;
  scheduled_publish_at?: null;
  is_shared?: boolean;
  updated_at?: string;
  edited_at?: string;
  translation_source?: null;
  trend_at?: null;
  promoted_at?: null;
  reading_time?: number;
  organization_pinned_at?: null;
  profile_pin_position?: null;
  points?: number;
  views_count?: number;
  clips_count?: number;
  comments_count?: number;
  rated_value?: null;
  promoted?: boolean;
  trending?: boolean;
  is_video?: boolean;
  thumbnail_url?: string;
  user?: IUser;
  tags?: ITags;
  commentators?: ICommentators;
}
export interface ICommentators {
  data?: Array<unknown>;
}
export interface ITags {
  data?: Array<IData1>;
}
export type IData1 = {
  slug?: string;
  name?: string;
};
export interface IUser {
  data?: IData;
}
export interface IData {
  id?: number;
  url?: string;
  avatar?: string;
  name?: string;
  username?: string;
  followers_count?: number;
  reputation?: number;
  posts_count?: number;
  banned_at?: null;
  level_partner?: null;
  following?: boolean;
}
