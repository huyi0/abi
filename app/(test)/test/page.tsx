'use client';
import { Screens } from '@/pattern/screens';
import { L } from 'libs/at';

export default function Page() {
  return (
    <L.NoSsr>
      <L.Core vertical width={'100%'} flex={1}>
        <Screens.TestTbl />
        <Screens.Chart />
      </L.Core>
    </L.NoSsr>
  );
}
