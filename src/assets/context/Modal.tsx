'use client';
import { $ModalKeys, ModalExports } from '@/collects/modals';
import { keys, map } from 'lodash';
import { Fragment, useState } from 'react';
import { onGumCtx } from './GumCtx';

type $ToggleFn = { key: $ModalKeys; data?: object };

type $Show = {
  [T in $ModalKeys]: {
    open: boolean;
    data?: object;
  };
};

export const { Provider, Consumer, use } = onGumCtx<
  ReturnType<typeof useLogic>
>({
  useLogic,
});

function useLogic() {
  const [show, setShow] = useState({} as $Show);

  const onToggle = (props: $ToggleFn) => {
    const { key, data } = props;
    setShow((prev) => ({
      ...prev,
      [key]: { ...prev[key], open: !prev[key]?.open, data },
    }));
  };

  const onRender = () => {
    const es = ModalExports();

    return map(keys(es), (key: $ModalKeys) => {
      const Render = es[key];
      return (
        show[key] && (
          <Fragment key={key}>
            <Render
              {...{
                ...show[key],
                onToggle: () => onToggle({ key }),
              }}
            />
          </Fragment>
        )
      );
    });
  };

  return { onToggle, show, setShow, extra: onRender };
}
