import { $Modal } from 'libs/es';
import { L } from 'libs/at';

export default function Index({ open, onToggle }: $Modal) {
  return (
    <L.Modal
      action={{ onToggle }}
      set={{
        open,
        isCloseOutside: true,
        header(props) {
          return <button onClick={props?.onToggle}>X</button>;
        },
        content: () => (
          <>
            <h2>Modal Content</h2>
            <p>This is the content of the modal.</p>
          </>
        ),
      }}
    />
  );
}
