'use client';
import { L } from 'libs/at';
import cx from 'classnames';
import { keys, map } from 'lodash';
import { useRouter } from 'next/navigation';
import styles from './styles.module.scss';

export default function Index() {
  const router = useRouter();

  const images = {
    before: '/card/before.svg',
    after: '/card/after.svg',
  };

  return (
    <L.Contain className={styles.Contact}>
      <L.Yard gap={16} vertical isScrolling flex={1} padding={16}>
        <L.Cover>
          <L.Btn kindType="shape" onClick={() => router.push('/edit-contact')}>
            <L.Txt>Edit card</L.Txt>
          </L.Btn>
        </L.Cover>

        <L.Section vertical gap={16}>
          <L.List id="listVisit" column={2} className={styles.listVisit}>
            {map(keys(images), (item: keyof typeof images) => {
              return (
                <L.Item
                  width={'fit-content'}
                  key={item}
                  className={cx(styles.item)}>
                  <L.Master
                    aspectRatio={1}
                    hover
                    shadow
                    borderRadius={8}
                    maxWidth={430}
                    maxHeight={650}
                    objectFit="contain"
                    scale={1}>
                    <picture>
                      <img src={images?.[item]} alt="" />
                    </picture>
                  </L.Master>
                </L.Item>
              );
            })}
          </L.List>
        </L.Section>
      </L.Yard>
    </L.Contain>
  );
}
