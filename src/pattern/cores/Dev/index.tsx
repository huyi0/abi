import { ModalCtx } from '@/assets/context';
import { Magic } from 'libs/es';

export default function Index() {
  const { onToggle } = ModalCtx.use();
  return (
    <>
      <Magic
        position="fixed"
        bottom={0}
        left={0}
        cursor="pointer"
        margin={8}
        width={16}
        height={16}
        onClick={() => onToggle({ key: 'Dev' })}
      />
    </>
  );
}
