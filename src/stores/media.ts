import { ctrl } from '@/process/ctrl';
import { $Media } from '@/assets/types/media';
import { StateCreator } from 'zustand';

const init = {
  medias: [],
  media: {},
  media_url: '',
};

export type State = {
  onMedias: () => Promise<void>;

  medias: $Media[];
  media: $Media;
  media_url: string;
  pickMedia(media: $Media): void;
};

export const mediaSlice: StateCreator<State> = (set) => {
  return {
    ...init,
    async onMedias() {
      const rs = await ctrl().onGetListFile({});
      set({
        medias: rs.data,
        media_url: rs.url,
      });
    },

    pickMedia(media: $Media) {
      set({ media });
    },
  } as const;
};
