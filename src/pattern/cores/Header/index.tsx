'use client';
import { sStoreCtx } from '@/assets/context';

import { Mode } from '@/assets/types';
import Drops from '@/collects/drop';
import List from '@/collects/list';
import { args } from '@/init';
import { L } from 'libs/at';
import { isEqual } from 'lodash';
import { Suspense } from 'react';
import styles from './styles.module.scss';
import Shapes from '@/pattern/shapes';

export default function Index() {
  const { onMode, mode, onGetListNotification } = sStoreCtx.use();

  const onSwitch = ({ isChecked }: any) => {
    onMode(isChecked ? Mode.dark : Mode.light);
    const body = document.body;
    body.classList.remove(Mode.light, Mode.dark);
    body.classList.add(isChecked ? Mode.dark : Mode.light);
  };

  return (
    <L.Core tag="header" className={styles.Header}>
      <L.Card vertical height={'100%'} overflow="auto">
        <L.Box padding={0} vertical gap={16}>
          <L.Cover justifyContent="space-between" alignItems="center">
            <L.Cover gap={16} alignItems="center">
              <L.Txt letterSpacing={3} fontSize={18} textTransform="uppercase">
                Hey you
              </L.Txt>
              <L.Dropbox
                isOutlet
                impact={(args) => {
                  return (
                    <L.Btn
                      kindType="shape"
                      onClick={() => {
                        args.onToggle();
                        onGetListNotification();
                      }}>
                      <L.Icon fontSize={16} className="fa-regular fa-bell" />
                    </L.Btn>
                  );
                }}
                contents={() => {
                  return <Drops.Notifications />;
                }}
              />
            </L.Cover>
            <Shapes.Battery size="3rem" />
            <L.Tick
              kind="switch"
              onTick={onSwitch}
              isChecked={isEqual(mode, Mode.light)}
              option={{ key: '', label: <L.Txt>Mode</L.Txt> }}
              theme={{
                border_color_active: 'lightblue',
                border_color: 'white',
              }}
            />
          </L.Cover>
          <L.Cover overflow="hidden" tag="picture" shadow>
            <picture style={{}}>
              <img src={args.env?.LOGO} alt="" />
            </picture>
          </L.Cover>
        </L.Box>
        <L.Cover justifyContent="center" alignItems="center">
          <L.Btn shadow backgroundColor={'lightskyblue'}>
            <L.Icon
              color="white"
              fontSize={'1.5rem'}
              className="fas fa-arrow-down"
            />
            <L.Txt color="white">Download my CV</L.Txt>
          </L.Btn>
        </L.Cover>
        <Suspense>
          <List.Menu />
        </Suspense>
      </L.Card>
    </L.Core>
  );
}
