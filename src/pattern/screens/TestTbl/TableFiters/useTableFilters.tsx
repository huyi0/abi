import { L } from "libs/at";
import { includes, size, toString } from "lodash";
import { ChangeEvent, useState } from "react";
import styles from "./styles.module.scss";

export type $FiltersData = {
  key: string;
  title?: string;
  type?:
    | "input"
    | "checkbox"
    | "radio"
    | "date"
    | "option"
    | "number"
    | "timedate";
  txtSearch?: any;
  items?: {
    key: string | number;
    label: string;
    value: any;
  }[];
  selectedOptions?: any[];
  isValue?: boolean;
};

export default function useTableFilters({ list }: { list: any[] }) {
  const init = {} as Record<string, Partial<$FiltersData>>;

  const [Filters, setFilters] = useState(init);

  const passesFilter = (item: any): boolean => {
    return Object.entries(Filters).every(([key, { txtSearch }]) => {
      const _key = toString(item[key]);
      const _search = toString(txtSearch);

      return includes(_key, _search);
    });
  };

  const listFilters_ = (): any[] => {
    return list?.reduce((acc: any[], item: any) => {
      const children = filterChildren(item?.children || []);
      if (passesFilter(item) || size(children) > 0) {
        acc.push({ ...item, children: size(children) > 0 ? children : "" });
      }
      return acc;
    }, []);
  };

  const filterChildren = (children: any[]): any[] =>
    children.reduce((acc: any[], child: any) => {
      const passesAllFilters = passesFilter(child);

      if (passesAllFilters) {
        acc.push({
          ...child,
          children: filterChildren(child.children || []),
        });
      }
      return acc;
    }, []);

  const renderFilter = (dataFilter: $FiltersData) => {
    const fitem = Filters[dataFilter.key];

    const onSearchTxt = (event: ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      setFilters((prev: any) => {
        return {
          ...prev,
          [dataFilter.key]: {
            ...prev[dataFilter.key],
            txtSearch: value,
            isValue: !!value,
          },
        };
      });
    };

    // const onClearTxt = () => {
    //   setFilters?.((prev: any) => {
    //     return {
    //       ...prev,
    //       [dataFilter.key]: {
    //         ...prev[dataFilter.key],
    //         txtSearch: '',
    //         isValue: false,
    //       },
    //     };
    //   });
    // };

    // const onReset = () => {
    //   setFilters?.((prev: any) => {
    //     return {
    //       ...prev,
    //       [dataFilter.key]: {
    //         ...prev[dataFilter.key],
    //         txtSearch: '',
    //         selectedOptions: null,
    //         isValue: false,
    //       },
    //     };
    //   });
    // };

    return {
      filterDropdown() {
        return (
          <L.Cover padding={0.5} minWidth={"220px"}>
            <L.Cover gap={0.3} className={styles.searchBar}>
              <input
                id={dataFilter.key}
                name={dataFilter.key}
                value={fitem?.txtSearch || ""}
                placeholder={`Tìm ${dataFilter.title}...`}
                onChange={onSearchTxt}
                autoFocus
                autoComplete="false"
              />
              {fitem?.txtSearch && <svg />}
            </L.Cover>
          </L.Cover>
        );
      },
    };
  };

  const listFilters = listFilters_();

  return {
    renderFilter,
    Filters,
    setFilters,
    listFilters,
  };
}
