'use client';
import { createContext, PropsWithChildren, ReactNode, use } from 'react';

export function onGumCtx<ArgsCtx, Props = unknown>({
  useLogic,
}: {
  useLogic: (props: Props) => ArgsCtx;
}) {
  const GumCtx = createContext({} as ArgsCtx);
  return {
    useLogic,
    use() {
      return use(GumCtx);
    },
    Provider({ ...props }: PropsWithChildren<Props>) {
      const logic = useLogic(props) as ArgsCtx & { extra?(): ReactNode };

      return (
        <GumCtx.Provider value={Object.assign({}, logic, props)}>
          {props?.children}
          {logic?.extra?.()}
        </GumCtx.Provider>
      );
    },
    Consumer({ ...props }: { children(args: ArgsCtx): ReactNode }) {
      return (
        <GumCtx.Consumer>
          {(args) => {
            return props?.children?.(args);
          }}
        </GumCtx.Consumer>
      );
    },
  };
}
