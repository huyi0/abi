import { ctrl } from '@/process/ctrl';
import { $Notification } from '@/assets/types/noti';
import { StateCreator } from 'zustand';

const init = {
  notifications: [],
  notification: {},
  subscriberId: '',
};

type $Noti = {
  data?: {
    [T: string]: string;
  };
} & { [T: string]: string };

export type State = {
  onGetListNotification: () => Promise<void>;
  selectNoti: ({ notification }: { notification: $Noti }) => void;
  setSubscriberId: (data: string) => void;
  onSendNotification: (payload: $Notification) => Promise<void>;
  notifications: $Noti[];
  notification: $Noti;
  subscriberId: string;
};

export const notiSlice: StateCreator<State> = (set, get) => {
  return {
    ...init,
    async onGetListNotification() {
      const rs = await ctrl().onGetNotifications();
      set({ notifications: rs.notifications });
    },
    selectNoti({ notification }: { notification: $Noti }) {
      set({ notification });
    },
    setSubscriberId(data: string) {
      set({ subscriberId: data });
    },
    async onSendNotification(payload: $Notification) {
      await ctrl().onSendNotification(payload);
      await get().onGetListNotification();
    },
  } as const;
};
