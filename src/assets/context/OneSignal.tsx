'use client';
import { Cards } from '@/collects/cards';
import { includes } from 'lodash';
import { cache, useEffect, useState } from 'react';
import OneSignal from 'react-onesignal';
import { toast } from 'react-toastify';
import { v4 } from 'uuid';
import { sStoreCtx } from '.';
import { onGumCtx } from './GumCtx';

const OneSignalAppId = {
  test: '0bfabb00-fec1-48ec-9e58-ff01fa217a16',
  live: '9dcb4b67-6e24-4eb5-9be6-a8e19b71f63d',
};

export const { Provider, Consumer, use } = onGumCtx<ReturnType<typeof useLogic>>({
  useLogic
});

function  useLogic() {
  const Id = v4();
  const { onGetListNotification, setSubscriberId } = sStoreCtx.use();
  const [enable, setEnable] = useState(false);

  const onInit = async (resolve: () => void) => {
    const isLocal = includes(window.location.origin, 'localhost');
    if (isLocal) return;

    if (!enable)
      await OneSignal.init({
        appId: OneSignalAppId.live,
        notifyButton: {
          enable: false,
        },
        // autoResubscribe: true,
        autoRegister: true,
        persistNotification: false,
        serviceWorkerPath: '/OneSignalSDKWorker.js',
        allowLocalhostAsSecureOrigin: true,
        path: '/',
      }).then(async () => {
        setEnable(true);
        await OneSignal.Notifications.requestPermission();
        await OneSignal.Slidedown.promptPush({
          force: true,
          forceSlidedownOverNative: true,
        });
        OneSignal.Notifications.isPushSupported();
        OneSignal.Debug.setLogLevel('');

        console.info('oneSignal init :>>', 'ready');
        resolve();
      });
  };

  const onLogin = async (resolve: () => void) => {
    await OneSignal.login(Id).then(() => {
      console.info('oneSignal login :>>', Id);
    });

    OneSignal.User.addAliases({
      Id,
    });
    resolve();
  };

  const onPushUser = async (resolve: () => void) => {
    let subscriberId;
    await setTimeout(async () => {
      try {
        await OneSignal.User.PushSubscription.optIn();
        subscriberId = await OneSignal.User.PushSubscription.id;
        await OneSignal.Slidedown.promptPush({
          force: true,
          forceSlidedownOverNative: true,
        });
        if (subscriberId) {
          setSubscriberId(subscriberId);
          console.info('oneSignal subscriberId :>> ', subscriberId);
        }
      } catch (error) {
        console.info('error getUserPush :>> ', error);
      }
      resolve();
    }, 0);

    return subscriberId;
  };

  const onListener = async () => {
    console.info('oneSignal listener :>> ', 'ready');

    OneSignal.Notifications.addEventListener(
      'foregroundWillDisplay',
      async (event) => {
        console.info('foregroundWillDisplay event :>> ', event);
        cache(onGetListNotification)();
        toast(<Cards.Notification item={event.notification} />, {
          autoClose: false,
        });
      },
    );

    OneSignal.Notifications.addEventListener('click', (event) => {
      console.info('click event :>> ', event);
    });
  };

  const onWorkOneSignal = async () => {
    await new Promise<void>(onInit);
    await new Promise<void>(onPushUser);
    await new Promise<void>(onLogin);
    await new Promise<void>(onListener);
  };

  useEffect(() => {
    onWorkOneSignal();
  }, []);

  useEffect(() => {
    cache(onGetListNotification)();
  }, [onGetListNotification]);

  return {};
}