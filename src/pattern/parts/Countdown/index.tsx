'use client';

import { L } from 'libs/at';
import { keys, map } from 'lodash';
import moment from 'moment';
import { memo } from 'react';
import { useCountDown } from './hook';

export default memo(function Index() {
  const { time } = useCountDown({
    timeThen: moment().endOf('days'),
  });

  type KeyParse = keyof typeof time.parse;

  return (
    <L.Block flexDirection="column">
      <L.Txt fontWeight={600} color="lightcoral">
        Thời gian còn lại trong ngày:
      </L.Txt>
      <L.Cover gap="0.5rem">
        {map(keys(time?.parse), (item: KeyParse, idx = 0) => {
          return (
            <>
              <L.Txt key={idx}>
                {item}: {time?.parse?.[item]}
              </L.Txt>
            </>
          );
        })}
      </L.Cover>
    </L.Block>
  );
});
