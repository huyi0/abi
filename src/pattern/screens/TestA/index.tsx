"use client";
import { L } from "libs/at";
import { memo } from "react";
import { FormProvider, useForm } from "react-hook-form";

export default memo(function Test() {
  const methods = useForm({ mode: "all" });

  const onSubmit = (data: any) => {
    console.info("data :>> ", data);
  };

  console.info("methods :>> ", methods.watch("fi"));

  return (
    <>
      <L.Contain>
        <L.Card>
          <FormProvider {...methods}>
            <L.Upload name="fi" />
            <L.Btn
              kindType="outline"
              onClick={methods.handleSubmit(onSubmit, (errors) =>
                console.info("errors :>> ", errors),
              )}>
              submit
            </L.Btn>
          </FormProvider>
        </L.Card>
      </L.Contain>
    </>
  );
});
