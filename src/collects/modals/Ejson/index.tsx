import { CtxEjson, sStoreCtx,  } from '@/assets/context';

import { Screens } from '@/pattern/screens';
import { L } from 'libs/at';
import { $Modal } from 'libs/es';
import styles from './styles.module.scss';

export default function Index({ open, onToggle, ...args }: $Modal) {
  return (
    <CtxEjson.Provider>
      <L.Modal
        action={{ onToggle }}
        set={{
          open,
          clsModal: styles.modal,
          clsOverlay: styles.overlay,
          isCloseOutside: true,

          header: (props) => <Header {...props} {...args} />,
          content: () => <Content />,
        }}
      />
    </CtxEjson.Provider>
  );
}

const Header = ({ ...props }: any) => {
  const { onCopyJson, onToggleCollapse } = CtxEjson.use();
  const { getJson } = sStoreCtx.use();

  return (
    <>
      <L.Block
        className={styles.btnRefresh}
        gap={16}
        justifyContent="space-between">
        <L.Txt fontSize={'1.5rem'} fontWeight={500} color="lightseagreen">
          EJson
        </L.Txt>
        <L.Cover gap={16}>
          <L.Btn
            kindType="outline"
            hover
            shadow={false}
            onClick={onToggleCollapse}
            width={'fit-content'}>
            Collapse
          </L.Btn>
          <L.Btn
            kindType="outline"
            hover
            shadow={false}
            onClick={onCopyJson}
            width={'fit-content'}>
            Copy
          </L.Btn>
          <L.Btn
            kindType="shape"
            hover
            shadow={false}
            onClick={getJson}
            width={'fit-content'}>
            Refresh
          </L.Btn>
          <L.Btn
            shadow={false}
            width={'fit-content'}
            background={'lightcoral'}
            hover
            onClick={props.onToggle}
            color="white">
            X
          </L.Btn>
        </L.Cover>
      </L.Block>
    </>
  );
};

const Content = () => {
  return (
    <>
      <Screens.EJson />
    </>
  );
};
