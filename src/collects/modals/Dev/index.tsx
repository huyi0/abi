import { sStoreCtx } from '@/assets/context';

import { args } from '@/init';
import { _on } from '@/process/ons';
import { L } from 'libs/at';
import { $Modal } from 'libs/es';
import { map } from 'lodash';
import { useRouter } from 'next/navigation';
import styles from './styles.module.scss';

export default function Index({ open, onToggle, ...args }: $Modal) {
  return (
    <L.Modal
      action={{ onToggle }}
      set={{
        clsOverlay: styles.overlay,
        clsModal: styles.modal,
        open,
        isCloseOutside: true,
        header: (props) => <Header {...props} {...args} />,
        content: () => <Content />,
      }}
    />
  );
}

function Header({ ...props }: any) {
  return (
    <L.Section
      shadow
      display="flex"
      gap={'1rem'}
      horizontal
      alignItems="center"
      justifyContent="space-between"
      padding={'1rem'}
      background={'lightsteelblue'}
      borderRadius={5}>
      <L.Txt
        flex={1}
        tag="p"
        fontSize={24}
        letterSpacing={1}
        justifyContent="center"
        color="white">
        Support Express For Dev
      </L.Txt>
      <L.Btn
        background={'lightcoral'}
        width={'fit-content'}
        color="white"
        shadow
        onClick={props.onToggle}>
        X
      </L.Btn>
    </L.Section>
  );
}

function Content() {
  const { resetJson, json } = sStoreCtx.use();

  const { push } = useRouter();

  const listBtn = [
    {
      label: 'Pipline',
      onClick: () => {
        window.open('https://gitlab.com/huyi0/abi/-/pipelines', 'blank');
      },
    },
    {
      label: 'Clear data',
      onClick: () => {
        localStorage.clear();
        sessionStorage.clear();
      },
    },
    {
      label: 'Clear Json',
      onClick: () => {
        resetJson();
      },
    },
    {
      label: 'Reset all',
      onClick: () => {
        localStorage.clear();
        location.reload();
      },
    },
    {
      label: 'Copy DB',
      onClick: () => {
        _on.Copy(JSON.stringify(json, undefined, 2));
      },
    },

    {
      label: 'Edit card vist',
      onClick: () => {
        push('/edit-contact');
      },
    },
  ];

  return (
    <L.Section height={'100%'} flex={1} vertical gap={16} marginTop={16}>
      <L.Block overflow="auto">
        <code>
          <pre>{JSON.stringify(args.env, undefined, 2)}</pre>
        </code>
      </L.Block>

      <L.Block height={'100%'} flex={1} margin={-16}>
        <L.Cover padding={16} isScrolling>
          <L.List column={2} flexWrap="wrap">
            <>
              {map(listBtn, (item, idx) => {
                return (
                  <L.Item key={idx}>
                    <L.Btn
                      flex={1}
                      padding={'1rem'}
                      shadow
                      onClick={item?.onClick || undefined}>
                      <L.Txt>{item.label}</L.Txt>
                    </L.Btn>
                  </L.Item>
                );
              })}
            </>
            <L.Item>
              <L.Dropbox
                {...{
                  setting: {
                    width: '100%',
                  },
                  clsDropbox: styles.clsDropbox,
                  impact(args) {
                    return (
                      <L.Magic
                        shadow
                        hover
                        flex={1}
                        borderRadius={5}
                        padding={'1rem'}
                        onClick={args.onToggle}
                        className={styles.impact}>
                        <L.Txt justifyContent="center">Toggle dropdown</L.Txt>
                      </L.Magic>
                    );
                  },
                  contents() {
                    return (
                      <L.Section
                        background={'white'}
                        minWidth={'auto'}
                        flex={0.5}
                        shadow
                        minHeight={'100%'}
                        padding={'1rem'}>
                        <L.Txt>contents</L.Txt>
                      </L.Section>
                    );
                  },
                }}
              />
            </L.Item>
          </L.List>
        </L.Cover>
      </L.Block>
    </L.Section>
  );
}
