'use client';
import { toString } from 'lodash';
import moment, { Moment } from 'moment';
import { useCallback, useEffect, useState } from 'react';

type $Parse = {
  months?: string;
  days?: string;
  hours?: string;
  minutes?: string;
  seconds?: string;
};

type $Time = {
  timeString: string;
  diffTimes: number;
  stop: boolean;
  parse: $Parse;
  initTime?: $Parse;
};

type $Props = {
  timeThen: Moment;
  initTime?: $Parse;
};

export function useCountDown({
  timeThen,
  initTime = {
    months: '00',
    days: '00',
    hours: '00',
    minutes: '00',
    seconds: '00',
  },
}: $Props) {
  const [time, setTime] = useState({ parse: initTime } as $Time);

  const padTime = (val: number) => {
    return toString(val).padStart(2, '0') as string;
  };

  const onConstrueTime = (timeThen: Moment) => {
    const now = moment();
    const then = moment(timeThen);

    const diffTimes = then.diff(now);

    const remaining = moment.duration(diffTimes);

    const months = padTime(remaining.months());
    const days = padTime(remaining.days());
    const hours = padTime(remaining.hours());
    const minutes = padTime(remaining.minutes());
    const seconds = padTime(remaining.seconds());

    return {
      timeString:
        `ngày: ${days} - giờ: ${hours} - phút :${minutes} - giây: ${seconds}`.toString(),
      diffTimes,
      stop: diffTimes < 1,
      parse: { months, days, hours, minutes, seconds },
    } as const;
  };

  const onCountdown = useCallback(() => {
    const interval = setInterval(() => {
      const result = onConstrueTime(timeThen);

      if (result?.stop) {
        clearInterval(interval);
      } else {
        setTime((prev) => ({
          ...result,
          parse: { ...initTime, ...prev.parse, ...result.parse },
        }));
      }
    }, 1000);
  }, [onConstrueTime, timeThen, initTime]);

  useEffect(() => {
    onCountdown();
  }, []);

  return { time };
}
