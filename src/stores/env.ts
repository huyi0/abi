import { $Env } from '@/process/env/tEnv';
import { StateCreator } from 'zustand';

export type $EnvSlice = {
  setEnv: (data: $Env) => void;
  getEnv: () => Promise<void>;
  reset: () => void;
  env: Partial<$Env>;
};

export const envSlice: StateCreator<$EnvSlice> = (set) => {
  return {
    env: {},
    setEnv: (data: $Env) => set({ env: data }),
    async getEnv() {
      try {
        // const env = await onGetEnv();
        // set({ env });
      } catch (error) {
        console.info('error :>> ', error);
      }
    },

    reset() {
      set({});
    },
  } as const;
};
