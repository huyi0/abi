'use client';
import { ModalCtx } from '@/assets/context';
import { L } from 'libs/at';
import { memo, useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

export default memo(function Test() {
  const methods = useForm({ mode: 'all' });

  const { onToggle } = ModalCtx.use();

  const [message, setMessage] = useState('');

  useEffect(() => {
    const channel = new BroadcastChannel('C');

    channel.onmessage = (event) => {
      console.info('event.data :>> ', event.data);
      setMessage(event.data);
    };

    return () => {
      channel.close();
    };
  }, []);

  return (
    <FormProvider {...methods}>
      <L.Contain>
        <L.Card maxWidth={'fit-content'}>
          <L.Btn
            width={'fit-content'}
            kindType="outline"
            onClick={() => {
              onToggle({ key: 'TestD' });
            }}>
            Open modal TestD
          </L.Btn>
          <L.Block>{JSON.stringify(message, undefined, 4)}</L.Block>
        </L.Card>
      </L.Contain>
    </FormProvider>
  );
});
