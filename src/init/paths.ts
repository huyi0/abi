export function path(root: string, segment: string) {
  return `${root}${segment}`;
}

export const Root = '/';

export const urlSeg = {
  Home: '/',
  Login: 'auth/login',
};
