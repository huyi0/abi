'use client';
import { L } from 'libs/at';
import { useEffect, useState } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

export default (function Test() {
  const methods = useForm({ mode: 'all' });
  const [message, setMessage] = useState('');

  useEffect(() => {
    const channel = new BroadcastChannel('myChannelB');

    channel.onmessage = (event) => {
      setMessage(event.data);
    };

    return () => {
      channel.close();
    };
  }, []);

  const sendMessage = () => {
    const channel = new BroadcastChannel('myChannelA');
    channel.postMessage('Hello from React A!');
  };

  return (
    <FormProvider {...methods}>
      <L.Contain>
        <L.Card>
          B:
          <>
            <code>{JSON.stringify(message)}</code>
          </>
          <L.Btn
            width={'fit-content'}
            kindType="outline"
            onClick={() => {
              sendMessage();
            }}>
            Add todos
          </L.Btn>
        </L.Card>
      </L.Contain>
    </FormProvider>
  );
});
