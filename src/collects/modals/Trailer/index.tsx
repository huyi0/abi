import YouTube, { YouTubeEvent } from 'react-youtube';

import { $Modal } from 'libs/es';
import { L } from 'libs/at';
import styles from './styles.module.scss';
import { toString } from 'lodash';
import { sStoreCtx } from '@/assets/context';


export default function Index({ open, onToggle }: $Modal) {
  return (
    <L.Modal
      action={{ onToggle }}
      set={{
        clsModal: styles.modal,
        clsOverlay: styles.overlay,
        open,
        isCloseOutside: true,
        header: (props) => {
          return <Header {...props} />;
        },
        content: () => {
          return <Content />;
        },
      }}
    />
  );
}

const Header = ({ ...props }: any) => {
  return (
    <L.Btn
      className={styles.btnClose}
      width={'fit-content'}
      background={'lightcoral'}
      shadow
      hover
      onClick={props.onToggle}
      color="white">
      X
    </L.Btn>
  );
};

const Content = () => {
  const { cinema } = sStoreCtx.use();
  const opts = {
    width: '0',

    playerVars: {
      autoplay: 1,
    },
  };

  const onReady = (event: YouTubeEvent) => {
    event.target.playVideo();
  };

  const urlParams = new URLSearchParams(new URL(cinema?.trailer).search);

  return (
    <>
      <YouTube
        videoId={toString(urlParams.get('v'))}
        opts={opts}
        onReady={onReady}
        className={styles.video}
      />
    </>
  );
};
